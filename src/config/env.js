/**
 * App configuration
 */
import 'dotenv/config'
import path from 'path'
import { toBoolean } from '#utils/helpers.js'

const env = {
    APP_TIMEZONE: process.env.APP_TIMEZONE,

    BOT_NAME: process.env.BOT_NAME,
    BOT_TOKEN: process.env.BOT_TOKEN,
    BOT_SHUTDOWN: toBoolean(process.env.BOT_SHUTDOWN),

    DOC_URL: process.env.DOC_URL,
    DOC_PATH: process.env.DOC_PATH,

    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
    DB_DATABASE: process.env.DB_DATABASE,
    DB_USERNAME: process.env.DB_USERNAME,
    DB_PASSWORD: process.env.DB_PASSWORD,
    DB_DEBUG: toBoolean(process.env.DB_DEBUG),
}

global.env = (key, def = null) => env[key] !== undefined ? env[key] : def

global.storagePath = dir => path.resolve(path.join('src/storage', dir || ''))
