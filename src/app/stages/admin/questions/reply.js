/**
 * Ask a question dialog
 */
import fs from 'fs'
import { Markup } from 'telegraf'

import { onInput } from '#scene'
import { adminScene } from '#scene'
import dayjs from '#utils/dayjs.js'
import { unique } from '#utils/random.js'
import { toHTML } from '#utils/entities.js'
import { Question } from '#models/question.js'

/** Dialog */
export async function answerDialog(ctx) {
    onInput(ctx, answerValue)

    return ['Составь ответ любого формата и объемом до 2 000 символов.']
}

/** Actions */
adminScene.action('question-answer-edit', async ctx => {
    if (!ctx.session._questionFilePath) {
        return ctx.reply(
            'Сообщение уже устарело.\n' +
            'Выполните команду /questions еще раз для создания ответа.'
        )
    }

    onInput(ctx, answerValue)

    return ctx.editMessageText(
        'Перепиши ответ и отправь заново с внесенными дополнениями.'
    )
})

adminScene.action('question-answer-send', async ctx => {
    if (!ctx.session._questionFilePath) {
        return ctx.reply(
            'Сообщение уже устарело.\n' +
            'Выполните команду /questions еще раз для просмотра всех вопросов.'
        )
    }

    onInput(ctx)

    let answer = null

    try {
        answer = fs.readFileSync(ctx.session._questionFilePath, { encoding: 'utf-8' })
    } catch (error) {
        console.error('Error to read question from file -', error.message)

        return ctx.reply('Произошла техническая ошибка, попробуйте отправить ответ позже.')
    }

    const question = await Question.findByPk(ctx.session.questionId)

    if (!question) {
        return ctx.reply('Не удалось найти вопрос и подобного произойти не должно было.')
    }

    await question.update({ answer })

    delete ctx.session.questionId

    try {
        await ctx.telegram.sendMessage(
            question.user_id, answer,
            {
                reply_to_message_id: question.message_id,
                parse_mode: 'HTML',
            }
        )
    } catch (error) {
        const createdAt = dayjs(question.created_at).format('DD.MM.YYYY')

        const messageInfo = await ctx.telegram.sendMessage(
            question.user_id,
            question.text +
            `\n\n👆 <i>Ваш личный вопрос от ${createdAt} г.</i>`,
            { parse_mode: 'HTML' }
        )

        await ctx.telegram.sendMessage(
            question.user_id, answer,
            {
                reply_to_message_id: messageInfo.message_id,
                parse_mode: 'HTML',
            }
        )

        await question.update({ message_id: null })
    }

    fs.unlinkSync(ctx.session._questionFilePath)

    delete ctx.session._questionFilePath

    return ctx.editMessageText('👍 Ответ успешно отправлен. ')
})

/** Messages */
export async function answerValue(ctx) {
    let value = ctx.message.text

    if (!value) {
        return ctx.reply('Ответ не может быть пустым.')
    }

    if (value.length > 2000) {
        return ctx.reply('Ответ не должен превышать 2 000 символов.')
    }

    onInput(ctx)

    value = toHTML(ctx.message)

    const questionFilePath = ctx.session._questionFilePath ||
        storagePath('app/' + unique())

    try {
        fs.writeFileSync(questionFilePath, value)
    } catch (error) {
        console.error('Error to write question to file -', error.message)

        return ctx.reply('Произошла техническая ошибка, попробуйте отправить ответ позже.')
    }

    ctx.session._questionFilePath = questionFilePath

    return ctx.reply(
        `Отправить ответ в текущем виде?`,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback('Дополнить', 'question-answer-edit'),
                    Markup.button.callback('Отправить »', 'question-answer-send'),
                ],
            ]),
            parse_mode: 'HTML',
        },
    )
}
