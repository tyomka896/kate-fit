/**
 * Asked question
 */
import { Markup } from 'telegraf'

import { adminScene } from '#scene'
import { Question } from '#models/question.js'
import { answerDialog } from './reply.js'

/** Keyboard */
export async function questionsKeyboard(ctx) {
    const questionsCount = await Question.count({ where: { answer: null } })

    if (questionsCount === 0) {
        return [
            'Список заданных вопросов, ожидающих ответа, пуст 🫠'
        ]
    }

    let page = 0
    let offset = 0

    if (ctx.match && /(-page-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.questionsPage) {
            page = +ctx.session.questionsPage
        }

        offset = page
    }

    const question = await Question.findOne({
        where: { answer: null },
        order: [ 'created_at' ],
        offset: offset,
        limit: 1,
    })

    const author = await question.getUser()

    return [
        `Вопрос от <b>${author.fullName()}</b>.\n\n` +
        question.text,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        '«',
                        `questions-page-${page - 1}`,
                        page === 0
                    ),
                    Markup.button.callback(
                        '»',
                        `questions-page-${page + 1}`,
                        1 >= questionsCount - offset
                    ),
                ],
                [ Markup.button.callback('Написать ответ', `question-answer-${question.id}`) ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/questions-page-(\d+)/, async ctx => {
    ctx.session.questionsPage = +ctx.match[1]

    return ctx.editMessageText(...await questionsKeyboard(ctx))
})

adminScene.action(/question-answer-(\d+)/, async ctx => {
    const question = await Question.findByPk(ctx.match[1])

    if (! question || question.answer) {
        return ctx.reply('Не удалось найти вопрос или на него уже дан ответ.')
    }

    ctx.session.questionId = ctx.match[1]

    const { text, entities } = ctx.update.callback_query.message

    await ctx.editMessageText(text, { entities })

    return ctx.reply(...await answerDialog(ctx))
})
