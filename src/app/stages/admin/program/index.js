/**
 * Program scene
 */
import { Markup } from 'telegraf'
import dayjs from '#utils/dayjs.js'

import { adminScene } from '#scene'
import { Chat } from '#models/chat.js'
import { getActiveWeek } from '#controllers/weekController.js'
import { weekProgram } from '../../../schedule/program.js'

/** Keyboard */
export async function programKeyboard(ctx) {
    const chat = await Chat.findOne({ where: { active: 1 } })

    const rows = [
        `В чат <b>${chat.name}</b> будет отправлено ` +
        'расписание тренировок на следующую неделю.'
    ]

    const pollOnSunday = dayjs().day() === 0 && getActiveWeek('MON')

    if (pollOnSunday) {
        rows.push(' Так же будет создан <b>опрос на понедельник</b>.')
    }

    rows.push('\n\n<i>Отправить расписание?</i>')

    return [
        rows.join(''),
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback('Нет', 'delete-message'),
                    Markup.button.callback('Да', 'program-send'),
                ],
            ]),
            parse_mode: 'HTML'
        },
    ]
}

/** Actions */
adminScene.action('program-send', async ctx => {
    await weekProgram()

    const pollOnSunday = dayjs().day() === 1 && getActiveWeek('MON')

    await ctx.editMessageText(
        'Расписание' +
        (pollOnSunday ? ' и опрос' : '') +
        ' успешно' +
        (pollOnSunday ? ' отправлены' : ' отправлено') +
        '.'
    )
})
