/**
 * Bonuses scene
 */
import { Markup } from 'telegraf'

import { adminScene } from '#scene'
import dayjs from '#utils/dayjs.js'
import { usersWithBonusAsText } from '#controllers/userHasPollController.js'

/** Keyboard */
export async function bonusesKeyboard(ctx) {
    const bonusesText = await usersWithBonusAsText()

    const today = dayjs().format('DD.MM.YYYY')

    return [
        (bonusesText || 'Ближайших бонусов не предвидится 🤷‍♂️') +
        `\n\n<i>${today} г.</i>`,
        {
            ...Markup.inlineKeyboard([
                [ Markup.button.callback('Обновить', 'bonuses-update') ],
            ]),
            parse_mode: 'HTML'
        },
    ]
}

/** Actions */
adminScene.action('bonuses-update', async ctx => {
    await ctx.editMessageText(...await bonusesKeyboard(ctx))
        .catch(() => {})

    await ctx.answerCbQuery()
})
