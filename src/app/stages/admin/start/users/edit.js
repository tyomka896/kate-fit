/**
 * User edit form
 */
import { Markup } from 'telegraf'

import { adminScene, onInput } from '#scene'
import { random } from '#utils/random.js'
import { backToModels } from '#utils/helpers.js'
import { User } from '#models/user.js'
import { deleteUser } from '#controllers/userController.js'

/** Keyboard */
export async function userEditKeyboard(ctx) {
    const model = await User.findByPk(ctx.session.userId)

    const emoji = model.id > 0 ? random([ '🙈', '🙉', '🙊' ]) : '🤖'

    return [
        `${emoji} <u>${model.linkName()}</u>\n\n` +
        `<b>Имя</b>: <i>${model.first_name}</i>\n` +
        `<b>Фамилия</b>: <i>${(model.last_name || '🚫')}</i>\n` +
        `<b>Честность</b>: <i>${model.integrity.toFixed(2)}</i>`,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        'Имя',
                        `user-edit-first-name-${model.id}`
                    ),
                    Markup.button.callback(
                        'Фамилия',
                        `user-edit-last-name-${model.id}`
                    ),
                ],
                [
                    Markup.button.callback(
                        'Удалить',
                        `user-edit-remove-${model.id}`
                    )
                ],
                [ Markup.button.callback('« Назад', 'back-to-users') ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/user-edit-first-name-(-?\d+)/, async ctx => {
    await ctx.answerCbQuery()

    if (! await User.findByPk(ctx.match[1])) {
        return backToModels(ctx, 'Пользователь не найден.', 'users')
    }

    onInput(ctx, userFirstName)

    ctx.session.userId = ctx.match[1]

    return ctx.reply('Введите новое имя пользователя.')
})

adminScene.action(/user-edit-last-name-(-?\d+)/, async ctx => {
    await ctx.answerCbQuery()

    if (! await User.findByPk(ctx.match[1])) {
        return backToModels(ctx, 'Пользователь не найден.', 'users')
    }

    onInput(ctx, userLastName)

    ctx.session.userId = ctx.match[1]

    return ctx.reply(
        'Введите новую фамилию пользователя.\n' +
        'Отправьте /empty, чтобы оставить пустым.'
    )
})

adminScene.action(/user-edit-remove-(-?\d+)/, async ctx => {
    const model = await User.findByPk(ctx.match[1])

    if (! model) {
        return backToModels(ctx, 'Пользователь не найден.', 'users')
    }

    return ctx.editMessageText(
        'Вы уверены, что хотите удалить пользователя?',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Нет', `user-model-${model.id}`),
                Markup.button.callback(
                    'Да',
                    `user-edit-remove-yes-${model.id}`
                ),
            ],
        ])
    )
})

adminScene.action(/user-edit-remove-yes-(-?\d+)/, async ctx => {
    const model = await User.findByPk(ctx.match[1])

    if (! model) {
        return backToModels(ctx, 'Пользователь не найден.', 'users')
    }

    return ctx.editMessageText(
        'Последний шанс подумать.\n' +
        'Может оставить пользователя?',
        Markup.inlineKeyboard([
            [
                Markup.button.callback(
                    'Нет',
                    `user-edit-remove-sure-yes-${model.id}`
                ),
                Markup.button.callback('Да', `user-model-${model.id}`),
            ],
        ])
    )
})

adminScene.action(/user-edit-remove-sure-yes-(-?\d+)/, async ctx => {
    const result = await deleteUser(ctx.match[1])

    if (! result) {
        return backToModels(ctx, 'Не удалось удалить пользователя.', 'users')
    }

    return backToModels(ctx, 'Мир толстяков стал немного больше 😉', 'users')
})

/** Messages */
export async function userFirstName(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Имя не может быть пустым.')
    }

    value = value.trim().replace(/\r?\n/g, '')

    if (value.length > 50) {
        return ctx.reply('Имя не должно превышать 50 символов.')
    }

    const model = await User.findByPk(ctx.session.userId)

    if (! model) {
        return backToModels(ctx, 'Пользователь не найден.', 'users', 0)
    }

    await model.update({ first_name: value })

    onInput(ctx)

    return ctx.reply(...await userEditKeyboard(ctx))
}

export async function userLastName(ctx) {
    const value = ctx.message.text ?
        ctx.message.text.trim().replace(/\r?\n/g, '') :
        null

    if (value && value.length > 50) {
        return ctx.reply('Фамилия не должна превышать 50 символов.')
    }

    const model = await User.findByPk(ctx.session.userId)

    if (! model) {
        return backToModels(ctx, 'Пользователь не найден.', 'users', 0)
    }

    await model.update({ last_name: value })

    onInput(ctx)

    return ctx.reply(...await userEditKeyboard(ctx))
}
