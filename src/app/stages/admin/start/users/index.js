/**
 * Users settings
 */
import { Markup } from 'telegraf'

import { adminScene } from '#scene'
import { User } from '#models/user.js'
import { userEditKeyboard } from './edit.js'
import { userCreateDialog } from './create.js'

/** Keyboard */
export async function usersKeyboard(ctx) {
    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (/(-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.usersPage) {
            page = +ctx.session.usersPage
        }

        offset = page * ROWS_PER_PAGE
    }

    const models = (await User.findAll({
        where: { role: 'user' },
        order: [
            [ 'last_name', 'nulls last' ],
            'first_name',
        ],
        offset: offset,
        limit: ROWS_PER_PAGE,
    }))
        .map(elem => (
            [ Markup.button.callback(
                elem.fullName(false),
                `user-model-${elem.id}`
            ) ]
        ))

    const modelsCount = await User.count({ where: { role: 'user' } })

    return [
        `Пользователи (${Math.min(offset + ROWS_PER_PAGE, modelsCount)}/${modelsCount})`,
        Markup.inlineKeyboard([
            ...models,
            [
                Markup.button.callback(
                    '«',
                    `users-page-${page - 1}`,
                    page === 0
                ),
                Markup.button.callback(
                    '»',
                    `users-page-${page + 1}`,
                    ROWS_PER_PAGE >= modelsCount - offset
                ),
            ],
            [
                Markup.button.callback('« Назад', 'back-to-start'),
                Markup.button.callback('Создать', 'user-create'),
            ],
        ])
    ]
}

/** Actions */
adminScene.action(/users-page-(\d+)/, async ctx => {
    ctx.session.usersPage = +ctx.match[1]

    return ctx.editMessageText(...await usersKeyboard(ctx))
})

adminScene.action(/user-model-(-?\d+)/, async ctx => {
    ctx.session.userId = +ctx.match[1]

    if (! await User.findByPk(ctx.session.userId)) {
        return ctx.editMessageText(...await usersKeyboard(ctx))
    }

    return ctx.editMessageText(...await userEditKeyboard(ctx))
})

adminScene.action('user-create', async ctx => {
    await ctx.answerCbQuery()

    await userCreateDialog(ctx)
})

adminScene.action('back-to-users', async ctx => {
    return ctx.editMessageText(...await usersKeyboard(ctx))
})
