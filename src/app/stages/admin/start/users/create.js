/**
 * User creation
 */
import { Markup } from 'telegraf'

import { onInput } from '#scene'
import { User } from '#models/user.js'
import { createUser } from '#controllers/userController.js'

/** Dialog */
export async function userCreateDialog(ctx) {
    onInput(ctx, userFirstName)

    await ctx.reply('Введите имя пользователя.')
}

/** Messages */
export async function userFirstName(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Имя не может быть пустым.')
    }

    value = value.trim().replace(/\r?\n/g, '')

    if (value.length > 50) {
        return ctx.reply('Имя не должно превышать 50 символов.')
    }

    ctx.session.userFirstName = value

    onInput(ctx, userLastName)

    return ctx.reply(
        'Введите фамилию пользователя.\n' +
        'Отправьте /empty, чтобы оставить пустым.'
    )
}

export async function userLastName(ctx) {
    const lastName = ctx.message.text ?
        ctx.message.text.trim().replace(/\r?\n/g, '') :
        null

    if (lastName && lastName.length > 50) {
        return ctx.reply('Фамилия не должна превышать 50 символов.')
    }

    const minId = await User.min('id')
    const firstName = ctx.session.userFirstName

    await createUser({
        id: minId >= 0 ? -1 : minId - 1,
        username: null,
        first_name: firstName,
        last_name: lastName,
    })

    onInput(ctx)

    delete ctx.session.userFirstName

    return ctx.reply(
        'Пользователь успешно создан.',
        Markup.inlineKeyboard([
            Markup.button.callback('« Назад', 'back-to-users'),
        ])
    )
}
