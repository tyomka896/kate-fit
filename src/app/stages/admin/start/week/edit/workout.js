/**
 * Workout selection for the week day
 */
import { Markup } from 'telegraf'
import { adminScene } from '#scene'
import { Week } from '#models/week.js'
import { Workout } from '#models/workout.js'
import { weekdayKeyboard } from './index.js'

/** Keyboard */
export async function weekEditWorkoutKeyboard(ctx) {
    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (/(-page-|back-to-)/.test(ctx.match[0])) {
        page = +ctx.match[1]
        offset = page * ROWS_PER_PAGE
    }

    const week = await Week.findOne({
        where: { id: ctx.session.weekId },
        include: 'workout',
    })

    const workouts = (await Workout.findAll({
        order: [ [ 'updated_at', 'desc' ] ],
        offset: offset,
        limit: ROWS_PER_PAGE,
    }))
        .map(elem => [
            Markup.button.callback(
                (week.workout?.id === elem.id ? '✓ ' : '') +
                elem.short_name,
                `week-edit-workout-model-${elem.id}`
            )
        ])

    const workoutsCount = await Workout.count()

    return [
        `Выбор тренировки`,
        {
            ...Markup.inlineKeyboard([
                ...workouts,
                [
                    Markup.button.callback(
                        '«',
                        `week-edit-workouts-page-${page - 1}`,
                        page === 0
                    ),
                    Markup.button.callback(
                        '»',
                        `week-edit-workouts-page-${page + 1}`,
                        ROWS_PER_PAGE >= workoutsCount - offset
                    ),
                ],
                [  ],
                [
                    Markup.button.callback('« Назад', `week-model-${week.id}`),
                    Markup.button.callback('Очистить', `week-edit-workout-model-0`),
                ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/week-edit-workouts-page-(\d+)/, async ctx => {
    return ctx.editMessageText(...await weekEditWorkoutKeyboard(ctx))
})

adminScene.action(/week-edit-workout-model-(\d+)/, async ctx => {
    const workoutId = +ctx.match[1]

    const workout = await Workout.findByPk(workoutId)

    const week = await Week.findByPk(ctx.session.weekId)

    await week.update({ workout_id: workout ? workout.id : null })

    return ctx.editMessageText(...await weekdayKeyboard(ctx))
})
