/**
 * Week day settings
 */
import { Markup } from 'telegraf'

import dayjs from '#utils/dayjs.js'
import { adminScene, onInput } from '#scene'
import { weekEditWorkoutKeyboard } from './workout.js'
import { Week } from '#models/week.js'
import { weekdayDescription } from '#controllers/weekController.js'

/** Keyboard */
export async function weekdayKeyboard(ctx) {
    const week = await Week.findOne({
        where: { id: ctx.session.weekId }
    }) || await Week.create({ id: ctx.session.weekId })

    const weekDayName = {
        'MON': 'Понедельник',
        'TU': 'Вторник',
        'WED': 'Среду',
        'TH': 'Четверг',
        'FRI': 'Пятницу',
        'SAT': 'Субботу',
        'SUN': 'Воскресенье',
    }[ctx.session.weekId]

    const startAt = ! week.time ? '🚫' : dayjs(week.time).tz().format('HH:mm')

    const hideExample = ! (week.workout_id && week.time && week.place)

    const workout = await week.getWorkout()

    return [
        `🗓 Тренировка в <b>${weekDayName}</b>\n\n` +
        `• время: <b>${startAt}</b>\n` +
        `• место: ${week.place || '🚫'}\n` +
        `• треним: <i>${workout?.name || '🚫'}</i>`,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        `${week.workout_id ? '✓' : '✗'} Тренировка`,
                        `week-edit-workout-${week.id}`
                    ),
                    Markup.button.callback(
                        `${week.time ? '✓' : '✗'} Время`,
                        `week-edit-time-${week.id}`
                    ),
                ],
                [
                    Markup.button.callback(
                        `${week.has_poll ? '✓' : '✗'} Опрос`,
                        `week-edit-poll-${week.id}`
                    ),
                    Markup.button.callback(
                        `${week.place ? '✓' : '✗'} Место`,
                        `week-edit-place-${week.id}`
                    ),
                ],
                [
                    Markup.button.callback(
                        'Пример описания',
                        `week-edit-example-${week.id}`,
                        hideExample
                    ),
                ],
                [ Markup.button.callback('« Назад', 'back-to-week') ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/week-edit-workout-(\w+)/, async ctx => {
    ctx.session.weekId = ctx.match[1]

    return ctx.editMessageText(...await weekEditWorkoutKeyboard(ctx))
})

adminScene.action(/week-edit-place-(\w+)/, async ctx => {
    await ctx.answerCbQuery()

    const model = await Week.findOne({
        where: { id: ctx.match[1] }
    })

    onInput(ctx, weekPlace)

    ctx.session.weekId = ctx.match[1]

    return ctx.reply(
        '<b>Сейчас</b>: ' +
        (model.place ? `<code>${model.place}</code>` : '🚫') + '\n\n' +
        'Введите место тренировки.\n' +
        'Отправьте /empty, чтобы оставить пустым.',
        { parse_mode: 'HTML' }
    )
})

adminScene.action(/week-edit-time-(\w+)/, async ctx => {
    await ctx.answerCbQuery()

    const model = await Week.findByPk(ctx.match[1])

    const startAt = ! model.time ? '🚫' : dayjs(model.time).tz().format('HH:mm')

    onInput(ctx, weekTime)

    ctx.session.weekId = ctx.match[1]

    return ctx.reply(
        '<b>Сейчас</b>: ' +
        `<code>${startAt}</code>` + '\n\n' +
        'Введите время тренировки в формате <code>00:00</code>.\n' +
        'Отправьте /empty, чтобы оставить пустым.',
        { parse_mode: 'HTML' }
    )
})

adminScene.action(/week-edit-poll-(\w+)/, async ctx => {
    const model = await Week.findByPk(ctx.match[1])

    await model.update({ has_poll: model.has_poll ? 0 : 1 })

    return ctx.editMessageText(...await weekdayKeyboard(ctx))
})

adminScene.action(/week-edit-example-(\w+)/, async ctx => {
    const model = await Week.findByPk(ctx.match[1])

    const description = await weekdayDescription(model)

    if (! description) {
        return ctx.editMessageText(...await weekdayKeyboard(ctx))
    }

    return ctx.editMessageText(description, {
        ...Markup.inlineKeyboard([
            [ Markup.button.callback('« Назад', `week-model-${model.id}`) ],
        ]),
        parse_mode: 'HTML',
    })
})

/** Messages */
export async function weekPlace(ctx) {
    const value = ctx.message.text

    if (value && value.length > 200) {
        return ctx.reply('Место тренировки не должно превышать 200 символов.')
    }

    const week = await Week.findByPk(ctx.session.weekId)

    await week.update({ place: ctx.message })

    onInput(ctx)

    return ctx.reply(...await weekdayKeyboard(ctx))
}

export async function weekTime(ctx) {
    const value = ctx.message.text ?
        dayjs(ctx.message.text + '+07:00', 'HH:mmZ') :
        null

    if (value && ! value.isValid()) {
        return ctx.reply('Формат времени не верный.')
    }

    const week = await Week.findByPk(ctx.session.weekId)

    await week.update({ time: value ? value.toDate() : null })

    onInput(ctx)

    return ctx.reply(...await weekdayKeyboard(ctx))
}
