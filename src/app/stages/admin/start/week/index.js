/**
 * Week days selection
 */
import { Markup } from 'telegraf'
import { adminScene } from '#scene'
import { Week } from '#models/week.js'
import { weekdayKeyboard } from './edit/index.js'
import { weekDescription } from '#controllers/weekController.js'

/** Keyboard */
export async function weekKeyboard(ctx) {
    const models = (await Week.findAll())
        .reduce((red, elem) => {
            red[elem.id] = elem

            return red
        }, {})

    let hideExample = true

    const weekButtons = [
        { key: 'MON', name: 'Понедельник' },
        { key: 'TU', name: 'Вторник' },
        { key: 'WED', name: 'Среда' },
        { key: 'TH', name: 'Четверг' },
        { key: 'FRI', name: 'Пятница' },
        { key: 'SAT', name: 'Суббота' },
        // { key: 'SUN', name: 'Воскресенье' },
    ]
        .map(elem => {
            elem.mark = '✗'

            if (models[elem.key]) {
                if (
                    models[elem.key].workout_id &&
                    models[elem.key].place &&
                    models[elem.key].time
                ) {
                    elem.mark = models[elem.key].has_poll ? '✓' : '●'

                    if (hideExample) {
                        hideExample = false
                    }
                }
            }

            return [
                Markup.button.callback(
                    `${elem.mark} ${elem.name}`,
                    `week-model-${elem.key}`
                )
            ]
        })

    return [
        'Расписание на неделю',
        Markup.inlineKeyboard([
            ...weekButtons,
            [
                Markup.button.callback(
                    'Пример расписания',
                    'week-model-example',
                    hideExample
                )
            ],
            [ Markup.button.callback('« Назад', 'back-to-start') ],
        ])
    ]
}

/** Actions */
adminScene.action(/week-model-(MON|TU|WED|TH|FRI|SAT|SUN)/, async ctx => {
    ctx.session.weekId = ctx.match[1]

    return ctx.editMessageText(...await weekdayKeyboard(ctx))
})

adminScene.action('week-model-example', async ctx => {
    const description = await weekDescription()

    if (! description) {
        return ctx.editMessageText(...await weekKeyboard(ctx))
    }

    return ctx.editMessageText(description, {
        ...Markup.inlineKeyboard([
            [ Markup.button.callback('« Назад', 'back-to-week') ],
        ]),
        parse_mode: 'HTML',
    })
})

adminScene.action('back-to-week', async ctx => {
    return ctx.editMessageText(...await weekKeyboard(ctx))
})
