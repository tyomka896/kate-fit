/**
 * Users settings
 */
import { Markup } from 'telegraf'

import { adminScene } from '#scene'
import dayjs from '#utils/dayjs.js'
import { firstCapital } from '#utils/helpers.js'
import { Poll } from '#models/poll.js'
import { pollKeyboard } from './edit.js'

/** Keyboard */
export async function pollsKeyboard(ctx) {
    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (/(-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.pollsPage) {
            page = +ctx.session.pollsPage
        }

        offset = page * ROWS_PER_PAGE
    }

    const models = (await Poll.findAll({
        order: [ [ 'end_at', 'desc' ] ],
        offset: offset,
        limit: ROWS_PER_PAGE,
    }))
        .map(elem => (
            [ Markup.button.callback(
                (elem.is_closed ? '' : '⧗ ') +
                firstCapital(dayjs(elem.end_at).format('dd, DD MMM, YYYY')),
                `poll-model-${elem.id}`
            ) ]
        ))

    const modelsCount = await Poll.count()

    return [
        `Опросы (${Math.min(offset + ROWS_PER_PAGE, modelsCount)}/${modelsCount})`,
        Markup.inlineKeyboard([
            ...models,
            [
                Markup.button.callback(
                    '«',
                    `polls-page-${page - 1}`,
                    page === 0
                ),
                Markup.button.callback(
                    '»',
                    `polls-page-${page + 1}`,
                    ROWS_PER_PAGE >= modelsCount - offset
                ),
            ],
            [ Markup.button.callback('« Назад', 'back-to-start') ],
        ])
    ]
}

/** Actions */
adminScene.action(/polls-page-(\d+)/, async ctx => {
    ctx.session.pollsPage = +ctx.match[1]

    return ctx.editMessageText(...await pollsKeyboard(ctx))
})

adminScene.action(/poll-model-(\d+)/, async ctx => {
    ctx.session.pollId = ctx.match[1]

    if (! await Poll.findByPk(ctx.session.pollId)) {
        return ctx.editMessageText(...await pollsKeyboard(ctx))
    }

    return ctx.editMessageText(...await pollKeyboard(ctx))
})

adminScene.action('back-to-polls', async ctx => {
    return ctx.editMessageText(...await pollsKeyboard(ctx))
})
