/**
 * User edit form
 */
import { Markup } from 'telegraf'

import { adminScene, onInput } from '#scene'
import { backToModels } from '#utils/helpers.js'
import { Poll } from '#models/poll.js'
import { Status } from '#models/status.js'
import { GlobalSetting, GLOBAL_SETTING_TYPE } from '#models/global_setting.js'
import {
    getStatusPoll,
    changePollPinStatus,
    cancelPoll,
    deletePoll,
} from '#controllers/pollController.js'
import { getAuth } from '#controllers/userController.js'
import { pollVotesKeyboard } from './votes/index.js'

/** Keyboard */
export async function pollKeyboard(ctx) {
    const model = await Poll.findByPk(ctx.session.pollId)

    const status = await getStatusPoll(model)

    const pinText = model.is_pinned ? 'Открепить' : 'Закрепить'

    return [
        status,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        'Изменить',
                        `poll-votes-${model.id}`,
                        !model.is_closed
                    ),
                    Markup.button.callback(
                        'Остановить',
                        `poll-stop-${model.id}`,
                        model.is_closed
                    ),
                    Markup.button.callback(
                        pinText,
                        `poll-pin-${model.id}`,
                        model.is_closed && !model.is_pinned
                    ),
                ],
                [
                    Markup.button.callback(
                        'Напомнить',
                        `poll-remind-${model.id}`,
                        model.is_closed || model.is_reminded,
                    ),
                    Markup.button.callback(
                        'Следить',
                        `poll-follow-${model.id}`,
                        model.is_closed,
                    ),
                ],
                [
                    Markup.button.callback('« Назад', 'back-to-polls'),
                    Markup.button.callback(
                        'Удалить',
                        `poll-delete-${model.id}`,
                        !model.is_closed
                    ),
                ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/poll-votes-(\d+)/, async ctx => {
    ctx.session.pollId = ctx.match[1]

    return ctx.editMessageText(...await pollVotesKeyboard(ctx))
})

adminScene.action(/poll-stop-(\d+)/, async ctx => {
    const model = await Poll.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Опрос не найден.', 'polls')
    }

    if (model.is_closed) {
        return ctx.editMessageText(...await pollKeyboard(ctx))
    }

    await ctx.answerCbQuery()

    ctx.session.pollId = ctx.match[1]

    onInput(ctx, cancelPollWithReason)

    return ctx.reply(
        'Введите причину остановки опроса.\n' +
        'Отправьте /empty, чтобы оставить пустым.',
    )
})

adminScene.action(/poll-pin-(\d+)/, async ctx => {
    const model = await Poll.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Опрос не найден.', 'polls')
    }

    return ctx.editMessageText(
        (model.is_pinned ? 'Открепить' : 'Закрепить') + ' опрос в группе?',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Нет', `poll-model-${model.id}`),
                Markup.button.callback('Да', `poll-pin-yes-${model.id}`),
            ],
        ])
    )
})

adminScene.action(/poll-pin-yes-(\d+)/, async ctx => {
    const model = await Poll.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Опрос не найден.', 'polls')
    }

    if (! await changePollPinStatus(ctx, model)) {
        await ctx.answerCbQuery()

        return ctx.editMessageText(
            'Не удалось закрепить сообщение.',
            Markup.inlineKeyboard([
                Markup.button.callback('« Назад', `poll-model-${model.id}`),
            ])
        )
    }

    return ctx.editMessageText(...await pollKeyboard(ctx))
})

adminScene.action(/poll-remind-(\d+)/, async ctx => {
    const model = await Poll.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Опрос не найден.', 'polls')
    }

    if (model.is_closed || model.is_reminded) {
        return ctx.editMessageText(...await pollKeyboard(ctx))
    }

    return ctx.editMessageText(
        'Напомнить группу про опрос?',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Нет', `poll-model-${model.id}`),
                Markup.button.callback('Да', `poll-remind-yes-${model.id}`),
            ],
        ])
    )
})

adminScene.action(/poll-remind-yes-(\d+)/, async ctx => {
    const poll = await Poll.findByPk(ctx.match[1])

    if (!poll) {
        return backToModels(ctx, 'Опрос не найден.', 'polls')
    }

    if (poll.is_closed || poll.is_reminded) {
        return ctx.editMessageText(...await pollKeyboard(ctx))
    }

    const reminder = (await GlobalSetting.findByPk(GLOBAL_SETTING_TYPE.POLL_REMINDER)).value

    try {
        await ctx.telegram.sendMessage(
            poll.chat_id,
            reminder,
            { reply_to_message_id: poll.message_id, parse_mode: 'HTML' },
        )
    } catch (error) {
        console.error(
            `Error to send reminder of the poll #${poll.id} ` +
            `to chat #${poll.chat_id} with message ` +
            `- ${error.message}.`
        )

        return ctx.editMessageText(
            '❗️Не удалось отправить напоминание в группу.',
            Markup.inlineKeyboard([
                [Markup.button.callback('« Назад', `poll-model-${poll.id}`)],
            ])
        )
    }

    const voters = await poll.getUsers()

    const private_reminder = (await GlobalSetting.findByPk(GLOBAL_SETTING_TYPE.POLL_PRIVATE_REMINDER)).value

    for (const voter of voters) {
        if (voter.UserHasPoll.values !== '[1]') {
            continue
        }

        await ctx.telegram.sendMessage(
            voter.id,
            private_reminder,
            {
                ...Markup.inlineKeyboard([
                    Markup.button.url('Перейти к опросу', poll.groupLink())
                ]),
                parse_mode: 'HTML',
            },
        )
            .catch(() => { })
    }

    await ctx.editMessageText(
        'Напоминание успешно отправлено.',
        Markup.inlineKeyboard([
            [Markup.button.callback('« Назад', `poll-model-${poll.id}`)],
        ])
    )

    await poll.update({ is_reminded: 1 })
})

adminScene.action(/poll-follow-(\d+)/, async ctx => {
    const model = await Poll.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Опрос не найден.', 'polls')
    }

    if (model.is_closed) {
        return ctx.editMessageText(...await pollKeyboard(ctx))
    }

    const currentStatus = await getStatusPoll(model)

    const markup = Markup.inlineKeyboard([
        [
            Markup.button.callback(
                'К опросу »',
                `poll-status-go-to-${model.id}`
            ),
        ],
    ])

    await ctx.editMessageText(
        currentStatus,
        { ...markup, parse_mode: 'HTML' },
    )

    const auth = await getAuth(ctx)

    await Status.create({
        user_id: auth.id,
        message_id: ctx.callbackQuery.message.message_id,
        poll_id: model.id,
    })
})

adminScene.action(/poll-delete-(\d+)/, async ctx => {
    const model = await Poll.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Опрос не найден.', 'polls')
    }

    return ctx.editMessageText(
        'Все результаты голосования будут утеряны,\n' +
        'сообщение будет откреплено и удалено в основном чате.\n\n' +
        '❓<i>Вы уверены, что хотите удалить опрос?</i>',
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback('Нет', `poll-model-${model.id}`),
                    Markup.button.callback('Да', `poll-delete-yes-${model.id}`),
                ],
            ]),
            parse_mode: 'HTML',
        }
    )
})

adminScene.action(/poll-delete-yes-(\d+)/, async ctx => {
    const result = await deletePoll(ctx, ctx.match[1])

    if (!result) {
        return backToModels(ctx, 'Не удалось удалить опрос.', 'polls')
    }

    return backToModels(ctx, 'Опрос успешно удален.', 'polls')
})

/** Messages */
export async function cancelPollWithReason(ctx) {
    const model = await Poll.findByPk(ctx.session.pollId)

    if (!model) {
        return backToModels(ctx, 'Опрос не найден.', 'polls', 0)
    }

    await cancelPoll(ctx, model)

    const value = ctx.message.text?.trim()

    if (value) {
        await ctx.telegram.sendMessage(
            model.chat_id,
            value,
            { reply_to_message_id: model.message_id }
        )
            .catch(() => { })
    }

    onInput(ctx)

    return ctx.reply(...await pollKeyboard(ctx))
}
