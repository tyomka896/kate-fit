/**
 * Poll users edit
 */
import { Markup } from 'telegraf'

import { adminScene } from '#scene'
import { Poll } from '#models/poll.js'
import dayjs from '#utils/dayjs.js'
import { firstCapital } from '#utils/helpers.js'
import { voteEditKeyboard } from './vote/index.js'
import { pollVotesAnotherKeyboard } from './another.js'

/** Keyboard */
export async function pollVotesKeyboard(ctx) {
    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (/(-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.votesPage) {
            page = +ctx.session.votesPage
        }

        offset = page * ROWS_PER_PAGE
    }

    const poll = await Poll.findByPk(ctx.session.pollId)

    const models = (await poll.getUsers({
        order: [
            [ 'last_name', 'nulls last' ],
            'first_name',
        ],
        offset: offset,
        limit: ROWS_PER_PAGE,
    }))
        .map(elem => (
            [ Markup.button.callback(
                elem.fullName(false),
                `poll-vote-${poll.id}-${elem.id}`
            ) ]
        ))

    const modelsCount = await poll.countUsers()
    const endAt = firstCapital(
        dayjs(poll.end_at)
            .format('dd, DD MMM, YYYY')
    )

    return [
        '<b>Участники опроса</b>\n' +
        `<i>${endAt}</i>`,
        {
            ...Markup.inlineKeyboard([
                ...models,
                [
                    Markup.button.callback(
                        '«',
                        `poll-votes-page-${page - 1}`,
                        page === 0
                    ),
                    Markup.button.callback(
                        '»',
                        `poll-votes-page-${page + 1}`,
                        ROWS_PER_PAGE >= modelsCount - offset
                    ),
                ],
                [
                    Markup.button.callback('« Назад', `poll-model-${poll.id}`),
                    Markup.button.callback(
                        'Добавить',
                        `poll-votes-another-${poll.id}`
                    ),
                ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/poll-votes-page-(\d+)/, async ctx => {
    ctx.session.votesPage = +ctx.match[1]

    return ctx.editMessageText(...await pollVotesKeyboard(ctx))
})

adminScene.action(/poll-vote-(\d+)-(-?\d+)/, async ctx => {
    ctx.session.pollId = ctx.match[1]
    ctx.session.voteUserId = ctx.match[2]

    return ctx.editMessageText(...await voteEditKeyboard(ctx))
})

adminScene.action(/poll-votes-another-(\d+)/, async ctx => {
    ctx.session.pollId = ctx.match[1]

    return ctx.editMessageText(...await pollVotesAnotherKeyboard(ctx))
})

adminScene.action('back-to-poll-votes', async ctx => {
    return ctx.editMessageText(...await pollVotesKeyboard(ctx))
})
