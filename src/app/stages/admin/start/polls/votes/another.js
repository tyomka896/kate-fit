/**
 * Poll users other edit
 */
import { Markup } from 'telegraf'
import { Op } from 'sequelize'

import { adminScene } from '#scene'
import dayjs from '#utils/dayjs.js'
import { firstCapital } from '#utils/helpers.js'
import { Poll } from '#models/poll.js'
import { User } from '#models/user.js'

/** Keyboard */
export async function pollVotesAnotherKeyboard(ctx) {
    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (/(-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.votesAPage) {
            page = +ctx.session.votesAPage
        }

        offset = page * ROWS_PER_PAGE
    }

    const poll = await Poll.findByPk(ctx.session.pollId)

    const pollUserIds = (await poll.getUsers({
        attributes: [ 'id' ],
    }))
        .map(elem => elem.id)

    const models = (await User.findAll({
        where: {
            id: { [Op.notIn]: pollUserIds },
        },
        order: [
            [ 'last_name', 'nulls last' ],
            'first_name',
        ],
        offset: offset,
        limit: ROWS_PER_PAGE,
    }))
        .map(elem => (
            [ Markup.button.callback(
                elem.fullName(false),
                `poll-vote-${poll.id}-${elem.id}`
            ) ]
        ))

    const modelsCount = await User.count({
        where: {
            id: { [Op.notIn]: pollUserIds },
        },
    })

    const endAt = firstCapital(
        dayjs(poll.end_at)
            .format('dd, DD MMM, YYYY')
    )

    return [
        '<b>Добавить участника</b>\n' +
        `<i>${endAt}</i>`,
        {
            ...Markup.inlineKeyboard([
                ...models,
                [
                    Markup.button.callback(
                        '«',
                        `poll-votes-another-page-${page - 1}`,
                        page === 0
                    ),
                    Markup.button.callback(
                        '»',
                        `poll-votes-another-page-${page + 1}`,
                        ROWS_PER_PAGE >= modelsCount - offset
                    ),
                ],
                [ Markup.button.callback('« Назад', 'back-to-poll-votes') ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/poll-votes-another-page-(\d+)/, async ctx => {
    ctx.session.votesAPage = +ctx.match[1]

    return ctx.editMessageText(...await pollVotesAnotherKeyboard(ctx))
})
