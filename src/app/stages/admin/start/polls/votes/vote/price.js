/**
 * Poll user price edit
 */
import { Markup } from 'telegraf'

import { adminScene, onInput } from '#scene'
import { Poll } from '#models/poll.js'
import { User } from '#models/user.js'
import { UserHasPoll } from '#models/user_has_poll.js'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { getAuth } from '#controllers/userController.js'
import { voteEditKeyboard } from './index.js'

/** Keyboard */
export async function votePriceEditKeyboard(ctx) {
    const auth = await getAuth(ctx)
    const poll = await Poll.findByPk(ctx.session.pollId)
    const user = await User.findByPk(ctx.session.voteUserId)

    let prices = await auth.getItemValue(
        SETTING_TYPE.PAYMENT,
        [[150, 300, 600]]
    )

    if (typeof prices === 'string') {
        prices = JSON.parse(prices)
    }

    prices = prices.map(elem =>
            elem.map(_elem =>
                Markup.button.callback(
                    _elem + ' ₽',
                    `poll-vote-edit-price-${poll.id}-${user.id}-${_elem}`
                )
            )
        )

    return [
        'Выберите размер оплаты',
        Markup.inlineKeyboard([
            ...prices,
            [
                Markup.button.callback(
                    '« Назад',
                    `poll-vote-${poll.id}-${user.id}`
                ),
                Markup.button.callback(
                    'Другая',
                    `poll-vote-price-another-${poll.id}-${user.id}`
                ),
            ]
        ])
    ]
}

/** Actions */
adminScene.action(/poll-vote-edit-price-(\d+)-(-?\d+)-(\d+)/, async ctx => {
    const model = await UserHasPoll.findOne({
        where: {
            poll_id: ctx.match[1],
            user_id: ctx.match[2],
        },
    })

    model.has_bonus = 0
    model.payment = +ctx.match[3]

    model.updateIntegrity()

    await model.save()

    return ctx.editMessageText(...await voteEditKeyboard(ctx))
})

adminScene.action(/poll-vote-price-another-(\d+)-(-?\d+)/, async ctx => {
    await ctx.answerCbQuery()

    ctx.session.pollId = ctx.match[1]
    ctx.session.voteUserId = ctx.match[2]

    onInput(ctx, anotherPrice)

    return ctx.reply('Введите размер оплаты.')
})

/** Messages */
export async function anotherPrice(ctx) {
    const model = await UserHasPoll.findOne({
        where: {
            poll_id: ctx.session.pollId,
            user_id: ctx.session.voteUserId,
        },
    })

    if (! model) {
        return ctx.reply('Что-то пошло не так, не удалось найти опрос.')
    }

    let value = ctx.message.text?.trim().replace(/\r?\n/g, '')

    if (! value) {
        await model.update({ payment: 0 })

        onInput(ctx)

        return ctx.reply(...await voteEditKeyboard(ctx))
    }

    if (isNaN(value) || +value > 100000) {
        return ctx.reply('Необходимо ввести числовое значение.')
    }

    model.updateIntegrity()

    await model.update({ payment: value })

    onInput(ctx)

    return ctx.reply(...await voteEditKeyboard(ctx))
}
