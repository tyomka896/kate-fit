/**
 * Poll user edit
 */
import { Markup } from 'telegraf'

import { adminScene } from '#scene'
import { Poll } from '#models/poll.js'
import { User } from '#models/user.js'
import { UserHasPoll } from '#models/user_has_poll.js'
import { GlobalSetting } from '#models/global_setting.js'
import { DEFAULT_ANSWERS } from '#controllers/pollController.js'
import { pollVotesKeyboard } from '../index.js'
import { votePriceEditKeyboard } from './price.js'

/** Keyboard */
export async function voteEditKeyboard(ctx) {
    const poll = await Poll.findByPk(ctx.session.pollId)
    const user = await User.findByPk(ctx.session.voteUserId)

    const userPollIds = {
        user_id: user.id,
        poll_id: poll.id,
    }

    const pollAnswer = await UserHasPoll.findOne({
        where: { ...userPollIds },
    }) || await UserHasPoll.create({
        ...userPollIds,
        values: '[]',
        payment: 0,
    })

    const answer = JSON.parse(pollAnswer.values)[0]

    const wasThere = pollAnswer.payment || pollAnswer.has_bonus
    const hasCome = `${(wasThere ? '✓' : '')} Был(а)`
    const hasNotCome = `${(wasThere ? '' : '✓')} Не был(а)`
    const hasBonus = `${(pollAnswer.has_bonus ? '✓' : '')} Бонусы`

    return [
        `<b>${user.fullName(false)}</b> ` +
        `<i>[${user.integrity.toFixed(1)}]</i>\n\n` +
        `<i>Ответ: ${DEFAULT_ANSWERS[answer] || '-'}</i>\n` +
        `<i>Оплата: ${pollAnswer.payment} ₽</i>`,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        hasCome,
                        `poll-vote-came-${poll.id}-${user.id}`
                    ),
                    Markup.button.callback(
                        hasNotCome,
                        `poll-vote-not-came-${poll.id}-${user.id}`
                    )
                ],
                [
                    Markup.button.callback(
                        hasBonus,
                        `poll-vote-bonus-${poll.id}-${user.id}`
                    ),
                    Markup.button.callback(
                        'Оплата',
                        `poll-vote-price-${poll.id}-${user.id}`
                    ),
                ],
                [
                    Markup.button.callback(
                        '« Назад',
                        `back-to-poll-votes-${poll.id}`
                    ),
                    Markup.button.callback(
                        'Скрыть',
                        `poll-vote-remove-${poll.id}-${user.id}`,
                        answer !== undefined
                    ),
                ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/poll-vote-came-(\d+)-(-?\d+)/, async ctx => {
    await ctx.answerCbQuery()

    const model = await UserHasPoll.findOne({
        where: {
            poll_id: ctx.match[1],
            user_id: ctx.match[2],
        },
    })

    if (! model) {
        return ctx.editMessageText(...await pollVotesKeyboard(ctx))
    }

    if (model.payment || model.has_bonus) return

    const pollPayment = (await GlobalSetting.findByPk('poll_payment')).value

    model.payment = model.has_bonus ? 0 : pollPayment

    model.updateIntegrity()

    await model.save()

    return ctx.editMessageText(...await voteEditKeyboard(ctx))
})

adminScene.action(/poll-vote-not-came-(\d+)-(-?\d+)/, async ctx => {
    await ctx.answerCbQuery()

    const model = await UserHasPoll.findOne({
        where: {
            poll_id: ctx.match[1],
            user_id: ctx.match[2],
        },
    })

    if (! model) {
        return ctx.editMessageText(...await pollVotesKeyboard(ctx))
    }

    if (! model.payment && ! model.has_bonus) return

    model.has_bonus = 0
    model.payment = 0

    model.updateIntegrity()

    await model.save()

    return ctx.editMessageText(...await voteEditKeyboard(ctx))
})

adminScene.action(/poll-vote-bonus-(\d+)-(-?\d+)/, async ctx => {
    const model = await UserHasPoll.findOne({
        where: {
            poll_id: ctx.match[1],
            user_id: ctx.match[2],
        },
    })

    if (! model) {
        return ctx.editMessageText(...await pollVotesKeyboard(ctx))
    }

    const pollPayment = (await GlobalSetting.findByPk('poll_payment')).value

    model.has_bonus = model.has_bonus === 0 ? 1 : 0
    model.payment = model.has_bonus ? 0 : pollPayment

    model.updateIntegrity()

    await model.save()

    return ctx.editMessageText(...await voteEditKeyboard(ctx))
})

adminScene.action(/poll-vote-price-(\d+)-(-?\d+)/, async ctx => {
    ctx.session.pollId = ctx.match[1]
    ctx.session.voteUserId = +ctx.match[2]

    return ctx.editMessageText(...await votePriceEditKeyboard(ctx))
})

adminScene.action(/poll-vote-remove-(\d+)-(-?\d+)/, async ctx => {
    const model = await UserHasPoll.findOne({
        where: {
            poll_id: ctx.match[1],
            user_id: ctx.match[2],
        },
    })

    if (! model) {
        return ctx.editMessageText(...await pollVotesKeyboard(ctx))
    }

    await model.destroy()

    delete ctx.session.voteUserId

    return ctx.editMessageText(...await pollVotesKeyboard(ctx))
})
