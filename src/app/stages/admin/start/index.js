/**
 * Main start scene
 */
import { Markup } from 'telegraf'
import { adminScene } from '#scene'
import { weekKeyboard } from './week/index.js'
import { workoutsKeyboard } from './workout/index.js'
import { usersKeyboard } from './users/index.js'
import { pollsKeyboard } from './polls/index.js'

/** Keyboard */
export async function startKeyboard(ctx) {
    return [
        'Главное меню',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Расписание', 'start-week') ],
            [ Markup.button.callback('Тренировки', 'start-workouts') ],
            [ Markup.button.callback('*Новостная лента', 'start-feed', true) ],
            [ Markup.button.callback('Опросы', 'start-polls') ],
            [ Markup.button.callback('Пользователи', 'start-users') ],
        ])
    ]
}

/** Actions */
adminScene.action('start-week', async ctx => {
    return ctx.editMessageText(...await weekKeyboard(ctx))
})

adminScene.action('start-workouts', async ctx => {
    return ctx.editMessageText(...await workoutsKeyboard(ctx))
})

adminScene.action('start-polls', async ctx => {
    return ctx.editMessageText(...await pollsKeyboard(ctx))
})

adminScene.action('start-users', async ctx => {
    return ctx.editMessageText(...await usersKeyboard(ctx))
})

adminScene.action('back-to-start', async ctx => {
    return ctx.editMessageText(...await startKeyboard(ctx))
})
