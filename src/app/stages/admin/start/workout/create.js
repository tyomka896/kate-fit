/**
 * Workout creation wizard
 */
import { Markup } from 'telegraf'

import { onInput } from '#scene'
import { Workout } from '#models/workout.js'
import { getAuth } from '#controllers/userController.js'

/** Dialog */
export async function createWorkoutDialog(ctx) {
    onInput(ctx, workoutName)

    return [ 'Введите полное наименование тренировки.' ]
}

/** Messages */
export async function workoutName(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Наименование не может быть пустым.')
    }

    value = value.replace(/\r?\n/g, '')

    if (value.length > 250) {
        return ctx.reply('Наименование не должно превышать 250 символов.')
    }

    ctx.session.workoutFullName = value

    onInput(ctx, workoutShortname)

    return ctx.reply('Введите сокращенное обозначение тренировки.')
}

export async function workoutShortname(ctx) {
    let shortName = ctx.message.text

    if (! shortName) {
        return ctx.reply('Сокращение не может быть пустым.')
    }

    shortName = shortName.replace(/\r?\n/g, '')

    if (shortName.length > 50) {
        return ctx.reply('Сокращение не должно превышать 50 символов.')
    }

    const modelExists = await Workout.count({
        where: { short_name: shortName }
    })

    if (modelExists) {
        return ctx.reply(`Тренировка с сокращением '${shortName}' уже существует.`)
    }

    const auth = await getAuth(ctx)
    const fullName = ctx.session.workoutFullName

    await Workout.create({
        user_id: auth.id,
        name: fullName,
        short_name: shortName,
    })

    onInput(ctx)

    delete ctx.session.workoutFullName

    return ctx.reply(
        'Тренировка успешно создана.',
        Markup.inlineKeyboard([
            Markup.button.callback('« Назад', 'back-to-workouts'),
        ])
    )
}
