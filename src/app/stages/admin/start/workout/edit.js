/**
 * Workout edit form
 */
import { Markup } from 'telegraf'

import { adminScene, onInput } from '#scene'
import { gender } from '#utils/emoji.js'
import { backToModels } from '#utils/helpers.js'
import { Workout } from '#models/workout.js'

/** Keyboard */
export async function workoutEditKeyboard(ctx) {
    const model = await Workout.findByPk(ctx.session.workoutId)

    return [
        `${gender()} ${model.name} ` +
        `(${model.short_name})\n\n` +
        '📝 <b>Описание</b>:' +
        (model.about ? `\n${model.about}` : ' 🚫') + '\n\n' +
        '👉 <b>Дополнительно</b>:' +
        (model.extra ? `\n${model.extra}` : ' 🚫'),
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        'Наименование',
                        `workout-edit-name-${model.id}`
                    ),
                    Markup.button.callback(
                        'Сокращение',
                        `workout-edit-short_name-${model.id}`
                    ),
                ],
                [
                    Markup.button.callback(
                        'Описание',
                        `workout-edit-about-${model.id}`
                    ),
                    Markup.button.callback(
                        'Дополнительно',
                        `workout-edit-extra-${model.id}`
                    ),
                ],
                [
                    Markup.button.callback(
                        'Удалить',
                        `workout-delete-${model.id}`
                    ),
                ],
                [Markup.button.callback('« Назад', 'back-to-workouts')],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
adminScene.action(/workout-edit-name-(\d+)/, async ctx => {
    await ctx.answerCbQuery()

    if (! await Workout.findByPk(ctx.match[1])) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts')
    }

    onInput(ctx, workoutName)

    ctx.session.workoutId = ctx.match[1]

    return ctx.reply('Введите новое наименование тренировки.')
})

adminScene.action(/workout-edit-short_name-(\d+)/, async ctx => {
    await ctx.answerCbQuery()

    if (! await Workout.findByPk(ctx.match[1])) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts')
    }

    onInput(ctx, workoutShortname)

    ctx.session.workoutId = ctx.match[1]

    return ctx.reply('Введите новое короткое название тренировки.')
})

adminScene.action(/workout-edit-about-(\d+)/, async ctx => {
    await ctx.answerCbQuery()

    if (! await Workout.findByPk(ctx.match[1])) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts')
    }

    onInput(ctx, workoutAbout)

    ctx.session.workoutId = ctx.match[1]

    return ctx.reply(
        'Введите новое описание тренировки.\n' +
        'Отправьте /empty, чтобы оставить пустым.'
    )
})

adminScene.action(/workout-edit-extra-(\d+)/, async ctx => {
    await ctx.answerCbQuery()

    if (! await Workout.findByPk(ctx.match[1])) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts')
    }

    onInput(ctx, workoutExtra)

    ctx.session.workoutId = ctx.match[1]

    return ctx.reply(
        'Введите новую дополнительную информацию к тренировке.\n' +
        'Отправьте /empty, чтобы оставить пустым.'
    )
})

adminScene.action(/workout-delete-(\d+)/, async ctx => {
    const model = await Workout.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts')
    }

    return ctx.editMessageText(
        `Вы уверены, что хотите удалить тренировку?`,
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Нет', `workout-model-${model.id}`),
                Markup.button.callback('Да', `workout-delete-yes-${model.id}`),
            ],
        ])
    )
})

adminScene.action(/workout-delete-yes-(\d+)/, async ctx => {
    const model = await Workout.findByPk(ctx.match[1])

    if (!model) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts')
    }

    await model.destroy()

    return backToModels(ctx, 'Тренировка успешно удалена.', 'workouts')
})

/** Messages */
export async function workoutName(ctx) {
    let value = ctx.message.text

    if (!value) {
        return ctx.reply('Наименование не может быть пустым.')
    }

    value = value.trim().replace(/\r?\n/g, '')

    if (value.length > 250) {
        return ctx.reply('Наименование не должно превышать 250 символов.')
    }

    const model = await Workout.findByPk(ctx.session.workoutId)

    if (!model) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts', 0)
    }

    await model.update({ name: value })

    onInput(ctx)

    return ctx.reply(...await workoutEditKeyboard(ctx))
}

export async function workoutShortname(ctx) {
    let value = ctx.message.text

    if (!value) {
        return ctx.reply('Короткое название не может быть пустым.')
    }

    value = value.trim().replace(/\r?\n/g, '')

    if (value.length > 50) {
        return ctx.reply('Короткое название не должно превышать 50 символов.')
    }

    const model = await Workout.findByPk(ctx.session.workoutId)

    if (!model) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts', 0)
    }

    await model.update({ short_name: value })

    onInput(ctx)

    return ctx.reply(...await workoutEditKeyboard(ctx))
}

export async function workoutAbout(ctx) {
    const value = ctx.message.text

    if (value && value.length > 500) {
        return ctx.reply('Описание не должно превышать 500 символов.')
    }

    const model = await Workout.findByPk(ctx.session.workoutId)

    if (!model) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts', 0)
    }

    await model.update({ about: ctx.message })

    onInput(ctx)

    return ctx.reply(...await workoutEditKeyboard(ctx))
}

export async function workoutExtra(ctx) {
    const value = ctx.message.text

    if (value && value.length > 500) {
        return ctx.reply('Дополнительная информация не должна превышать 500 символов.')
    }

    const model = await Workout.findByPk(ctx.session.workoutId)

    if (!model) {
        return backToModels(ctx, 'Тренировка не найдена.', 'workouts', 0)
    }

    await model.update({ extra: ctx.message })

    onInput(ctx)

    return ctx.reply(...await workoutEditKeyboard(ctx))
}
