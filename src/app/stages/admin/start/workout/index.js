/**
 * Workout settings
 */
import { Markup } from 'telegraf'

import { adminScene } from '#scene'
import { Workout } from '#models/workout.js'
import { workoutEditKeyboard } from './edit.js'
import { createWorkoutDialog } from './create.js'

/** Keyboard */
export async function workoutsKeyboard(ctx) {
    const ROWS_PER_PAGE = 5

    let page = 0
    let offset = 0

    if (/(-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.workoutsPage) {
            page = +ctx.session.workoutsPage
        }

        offset = page * ROWS_PER_PAGE
    }

    const models = (await Workout.findAll({
        order: [ [ 'updated_at', 'desc' ] ],
        offset: offset,
        limit: ROWS_PER_PAGE,
    }))
        .map(elem => (
            [
                Markup.button.callback(
                    elem.short_name,
                    `workout-model-${elem.id}`
                )
            ]
        ))

    const modelsCount = await Workout.count()

    return [
        `Тренировки (${Math.min(offset + ROWS_PER_PAGE, modelsCount)}/${modelsCount})`,
        Markup.inlineKeyboard([
            ...models,
            [
                Markup.button.callback(
                    '«',
                    `workouts-page-${page - 1}`,
                    page === 0
                ),
                Markup.button.callback(
                    '»',
                    `workouts-page-${page + 1}`,
                    ROWS_PER_PAGE >= modelsCount - offset
                ),
            ],
            [
                Markup.button.callback('« Назад', 'back-to-start'),
                Markup.button.callback('Создать', 'workout-create'),
            ],
        ])
    ]
}

/** Actions */
adminScene.action(/workouts-page-(\d+)/, async ctx => {
    ctx.session.workoutsPage = ctx.match[1]

    return ctx.editMessageText(...await workoutsKeyboard(ctx))
})

adminScene.action(/workout-model-(\d+)/, async ctx => {
    ctx.session.workoutId = ctx.match[1]

    if (! await Workout.findByPk(ctx.match[1])) {
        return ctx.editMessageText(...await workoutsKeyboard(ctx))
    }

    return ctx.editMessageText(...await workoutEditKeyboard(ctx))
})

adminScene.action('workout-create', async ctx => {
    await ctx.answerCbQuery()

    return ctx.reply(...await createWorkoutDialog(ctx))
})

adminScene.action('back-to-workouts', async ctx => {
    return ctx.editMessageText(...await workoutsKeyboard(ctx))
})
