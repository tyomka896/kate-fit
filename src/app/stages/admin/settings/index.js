/**
 * Settings
 */
import { Markup } from 'telegraf'
import { adminScene, onInput } from '#scene'
import { GlobalSetting, GLOBAL_SETTING_TYPE } from '#models/global_setting.js'
import { pollSettingsKeyboard } from './polls/index.js'

/** Keyboard */
export async function settingsKeyboard(ctx) {
    const payment = (await GlobalSetting.findByPk(GLOBAL_SETTING_TYPE.POLL_PAYMENT)).value

    return [
        'Настройки',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Опросы', 'settings-polls') ],
            [
                Markup.button.callback(
                    `Оплата (${payment})`,
                    'settings-global-poll-payment'
                ),
            ],
        ])
    ]
}

/** Actions */
adminScene.action('settings-global-poll-payment', async ctx => {
    await ctx.answerCbQuery()

    onInput(ctx, paymentValue)

    const payment = (await GlobalSetting.findByPk(GLOBAL_SETTING_TYPE.POLL_PAYMENT)).value

    return ctx.reply(
        'Введите новое значение, чтобы изменить.\n\n' +
        `<i>Текущее значение:</i> ${payment}₽`,
        { parse_mode: 'HTML' }
    )
})

adminScene.action('settings-polls', async ctx => {
    return ctx.editMessageText(...await pollSettingsKeyboard(ctx))
})

adminScene.action('back-to-settings', async ctx => {
    return ctx.editMessageText(...await settingsKeyboard(ctx))
})

/** Messages */
export async function paymentValue(ctx) {
    const value = parseInt(ctx.message.text)

    if (! value) {
        return ctx.reply('Введите числовое значение.')
    }

    const model = await GlobalSetting.findByPk(GLOBAL_SETTING_TYPE.POLL_PAYMENT)

    await model.update({ value })

    onInput(ctx)

    return ctx.reply(...await settingsKeyboard(ctx))
}
