/**
 * Poll settings
 */
import { Markup } from 'telegraf'

import { adminScene, onInput } from '#scene'
import { hiddenTextLink } from '#utils/helpers.js'
import { toHTML } from '#utils/entities.js'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { GlobalSetting, GLOBAL_SETTING_TYPE } from '#models/global_setting.js'
import { getAuth } from '#controllers/userController.js'
import { pollNotifySettingsKeyboard } from './notify.js'

/** Keyboard */
export async function pollSettingsKeyboard(ctx) {
    return [
        'Настройки опроса',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Уведомления', 'setting-poll-notify') ],
            [ Markup.button.callback('Кнопки оплаты', 'setting-poll-prices') ],
            [ Markup.button.callback('Текст напоминания', 'setting-poll-reminder') ],
            [ Markup.button.callback('« Назад', 'back-to-settings') ],
        ])
    ]
}

/** Actions */
adminScene.action('setting-poll-notify', async ctx => {
    return ctx.editMessageText(...await pollNotifySettingsKeyboard(ctx))
})

adminScene.action('setting-poll-prices', async ctx => {
    await ctx.answerCbQuery()

    onInput(ctx, pricesValues)

    return ctx.reply(
        'Введите диапазон значений со знаком / в качестве разделителя.\n\n' +
        '<i>Например:</i>' +
        hiddenTextLink('doc.tyomka896.ru/katefit/common/setting-poll-prices.png'),
        { parse_mode: 'HTML' }
    )
})

adminScene.action('setting-poll-reminder', async ctx => {
    await ctx.answerCbQuery()

    onInput(ctx, reminderValue)

    const reminder = (await GlobalSetting.findByPk(GLOBAL_SETTING_TYPE.POLL_REMINDER)).value

    return ctx.reply(
        'Введите текст напоминания об опросе.\n\n' +
        '<i>Текущее значение:</i>\n' +
        `• ${reminder}`,
        { parse_mode: 'HTML' }
    )
})

adminScene.action('back-to-poll-settings', async ctx => {
    return ctx.editMessageText(...await pollSettingsKeyboard(ctx))
})

/** Messages */
export async function pricesValues(ctx) {
    const auth = await getAuth(ctx)

    let values = ctx.message.text

    if (! values) {
        await auth.setItem(SETTING_TYPE.PAYMENT, [[150, 300, 600]])

        onInput(ctx)

        return ctx.reply(
            'Значения цен выставлены по умолчанию.',
            Markup.inlineKeyboard([
                [ Markup.button.callback('« Назад', 'back-to-poll-settings') ],
            ])
        )
    }

    values = values.split(/\r?\n/g)

    if (values.length > 3) {
        return ctx.reply('Допускается не больше трех строк.')
    }

    for (let l = 0; l < values.length; l++) {
        const _mapped = values[l].split('/')

        if (_mapped.length > 4) {
            return ctx.reply(
                `В ${l + 1}-й строке превышено количество цен.\n` +
                'Допускается не более четырех значений на строку.'
            )
        }

        for (let p = 0; p < _mapped.length; p++) {
            _mapped[p] = +_mapped[p]

            if (isNaN(_mapped[p]) || _mapped[p] === 0 || _mapped[p] > 100000) {
                return ctx.reply(
                    `Не все значения в ${l + 1}-й строке верны.\n` +
                    'Допускается только числовые значения.'
                )
            }
        }

        values[l] = _mapped
    }

    await auth.setItem(SETTING_TYPE.PAYMENT, values)

    onInput(ctx)

    return ctx.reply(
        'Значения цен успешно изменены.',
        Markup.inlineKeyboard([
            [ Markup.button.callback('« Назад', 'back-to-poll-settings') ],
        ])
    )
}

export async function reminderValue(ctx) {
    let value = toHTML(ctx.message)

    if (! value) {
        return ctx.reply('Введите текстовое значение.')
    }

    const model = await GlobalSetting.findByPk(GLOBAL_SETTING_TYPE.POLL_REMINDER)

    await model.update({ value })

    onInput(ctx)

    return ctx.reply(...await pollSettingsKeyboard(ctx))
}
