/**
 * Poll notify settings
 */
import { Markup } from 'telegraf'

import { adminScene } from '#scene'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function pollNotifySettingsKeyboard(ctx) {
    const auth = await getAuth(ctx)

    const statusItem = await auth.getItemValue(SETTING_TYPE.STATUS, true)
    const remindItem = await auth.getItemValue(SETTING_TYPE.REMIND, true)

    return [
        'Уведомления опроса',
        Markup.inlineKeyboard([
            [
                Markup.button.callback(
                    `${+statusItem ? '✓' : '✗'} Результаты`,
                    'setting-poll-status'
                )
            ],
            [
                Markup.button.callback(
                    `${+remindItem ? '✓' : '✗'} Напоминание`,
                    'setting-poll-remind'
                )
            ],
            [ Markup.button.callback('« Назад', 'back-to-poll-settings') ],
        ])
    ]
}

/** Actions */
adminScene.action('setting-poll-status', async ctx => {
    const auth = await getAuth(ctx)

    const statusItem = await auth.getItemValue(SETTING_TYPE.STATUS, true)

    await auth.setItem(SETTING_TYPE.STATUS, ! +statusItem)

    return ctx.editMessageText(...await pollNotifySettingsKeyboard(ctx))
})

adminScene.action('setting-poll-remind', async ctx => {
    const auth = await getAuth(ctx)

    const remindItem = await auth.getItemValue(SETTING_TYPE.REMIND, true)

    await auth.setItem(SETTING_TYPE.REMIND, ! +remindItem)

    return ctx.editMessageText(...await pollNotifySettingsKeyboard(ctx))
})
