/**
 * Main stage
 */
import { adminScene } from '#scene'
import help from '../../commands/help.js'
import { bonusesKeyboard } from './bonuses/index.js'
import { certsKeyboard } from '../common/certs/index.js'
import { foodHygieneDialog } from '../common/food_hygiene/index.js'
import { programKeyboard } from './program/index.js'
import { questionDialog } from '../common/question/index.js'
import { questionsKeyboard } from './questions/index.js'
import { settingsKeyboard } from './settings/index.js'
import { startKeyboard } from './start/index.js'
import { statusKeyboard } from './status/index.js'

/** Actions */
adminScene.enter(async ctx => {
    switch (ctx.scene.state.act) {
        case 'bonuses': return ctx.reply(...await bonusesKeyboard(ctx))
        case 'certs': return ctx.reply(...await certsKeyboard(ctx))
        case 'food_hygiene': return ctx.reply(...await foodHygieneDialog(ctx))
        case 'program': return ctx.reply(...await programKeyboard(ctx))
        case 'question': return ctx.reply(...await questionDialog(ctx))
        case 'questions': return ctx.reply(...await questionsKeyboard(ctx))
        case 'settings': return ctx.reply(...await settingsKeyboard(ctx))
        case 'start': return ctx.reply(...await startKeyboard(ctx))
        case 'status': return ctx.reply(...await statusKeyboard(ctx))
        default: return help(ctx)
    }
})

export { adminScene }
