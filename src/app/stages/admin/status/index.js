/**
 * Status scene
 */
import { Op } from 'sequelize'
import { Markup } from 'telegraf'
import schedule from 'node-schedule'

import dayjs from '#utils/dayjs.js'
import { adminScene } from '#scene'
import { User } from '#models/user.js'
import { Poll } from '#models/poll.js'
import { UserHasPoll } from '#models/user_has_poll.js'

/** Keyboard */
export async function statusKeyboard(ctx) {
    let rows = []
    const params = ctx.message?.text.split(' ').splice(1) || []

    if (params.length > 0) {
        const _command = params[0].toString().toLowerCase()

        if (_command === 'users') {
            let _limit =
                params[1] && params[1] > 0 && params[1] <= 10 ?
                params[1] : 3

            let _users = await User.findAll({
                where: { role: 'user' },
                order: [ [ 'created_at', 'desc' ] ],
                limit: _limit,
            })

            _users = _users.map(elem =>
                `• ${elem.fullName()}\n` +
                `  <i>${dayjs(elem.created_at).format()}</i>\n`
            )

            rows.push(
                `<b>Last ${_limit} users</b>:\n\n` +
                _users.join('')
            )
        }
        else if (_command === 'jobs') {
            const _jobs = Object.keys(schedule.scheduledJobs)
                .map(elem => `• ${elem}\n`)

            rows.push(
                '<b>List of jobs</b>:\n\n' +
                _jobs.join('')
            )
        }

        if (rows.length > 0) {
            return [
                rows.join(''),
                { parse_mode: 'HTML' },
            ]
        }
    }

    const fairVisits = await UserHasPoll.count({
        where: {
            [Op.or]: [
                { payment: { [Op.gt]: 0 } },
                { has_bonus: 1 },
            ],
        },
    })

    rows = [
        `<b>Users</b> : ${await User.count({ where: { role: 'user' } })}\n`,
        `<b>Visits</b> : ${fairVisits}/${await UserHasPoll.count()}\n`,
        `<b>Polls</b>  : ${await Poll.count()}\n`,
        `<b>Jobs</b>   : ${Object.keys(schedule.scheduledJobs).length}`,
    ]

    return [
        rows.join(''),
        {
            ...Markup.inlineKeyboard([
                [ Markup.button.callback('Обновить', 'status-update') ],
            ]),
            parse_mode: 'HTML'
        },
    ]
}

/** Actions */
adminScene.action('status-update', async ctx => {
    await ctx.editMessageText(...await statusKeyboard(ctx))
        .catch(() => {})

    await ctx.answerCbQuery()
})
