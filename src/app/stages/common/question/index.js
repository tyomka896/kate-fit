/**
 * Ask a question dialog
 */
import fs from 'fs'
import { Markup } from 'telegraf'

import { allScenes, onInput } from '#scene'
import { unique } from '#utils/random.js'
import { toHTML } from '#utils/entities.js'
import { User } from '#models/user.js'
import { Question } from '#models/question.js'
import { getAuth } from '#controllers/userController.js'

/** Dialog */
export async function questionDialog(ctx) {
    // const auth = await getAuth(ctx)

    // if (+auth.id === 1211871972) {
    //     return [
    //         'Ботя умный и он понимает, что самому себе задавать вопрос не к чему 🤪'
    //     ]
    // }

    onInput(ctx, questionValue)

    return [
        'Сформулируйте и по желанию раскройте волнующий вас ' +
        '<b>личный вопрос любой сложности и длины</b> как можно подробнее.\n\n' +
        '👉 <i>Любые данные являются конфиденциальными и публично раскрываться не будут</i>.',
        { parse_mode: 'HTML' },
    ]
}

/** Actions */
allScenes('question-create-edit', async ctx => {
    if (! ctx.session._questionFilePath) {
        return ctx.reply(
            'Сообщение уже устарело.\n' +
            'Выполните команду /question еще раз для создания вопроса.'
        )
    }

    onInput(ctx, questionValue)

    return ctx.editMessageText(
        'Перепишите вопрос и отправьте заново с внесенными дополнениями.'
    )
})

allScenes('question-create-send', async ctx => {
    if (! ctx.session._questionFilePath) {
        return ctx.reply(
            'Сообщение уже устарело.\n' +
            'Выполните команду /question еще раз для создания вопроса.'
        )
    }

    onInput(ctx)

    let text = null

    try {
        text = fs.readFileSync(ctx.session._questionFilePath, { encoding: 'utf-8' })
    } catch (error) {
        console.error('Error to read question from file -', error.message)

        return ctx.reply('Произошла техническая ошибка, попробуйте отправить вопрос позже.')
    }

    const auth = await getAuth(ctx)

    await Question.create({
        user_id: auth.id,
        message_id: ctx.update.callback_query.message.message_id - 1,
        text,
    })

    await ctx.editMessageText(
        '👍 Вопрос успешно отправлен. ' +
        'Ответ будет прислан в текущем боте вам лично.'
    )

    fs.unlinkSync(ctx.session._questionFilePath)

    delete ctx.session._questionFilePath

    const KateFitKv = await User.findByPk(1211871972) // 501244780 | 1211871972

    return ctx.telegram.sendMessage(
        KateFitKv.id,
        `Задан личный вопрос от <b>${auth.fullName()}</b>.\n\n` +
        '<i>Для просмотра отправьте /questions</i>.',
        { parse_mode: 'HTML' }
    )
})

/** Messages */
export async function questionValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Вопрос не может быть пустым.')
    }

    if (value.length > 2000) {
        return ctx.reply('Вопрос не должен превышать 2 000 символов.')
    }

    onInput(ctx)

    value = toHTML(ctx.message)

    const questionFilePath = ctx.session._questionFilePath ||
        storagePath('app/' + unique())

    try {
        fs.writeFileSync(questionFilePath, value)
    } catch (error) {
        console.error('Error to write question to file -', error.message)

        return ctx.reply('Произошла техническая ошибка, попробуйте отправить вопрос позже.')
    }

    ctx.session._questionFilePath = questionFilePath

    return ctx.reply(
        `Отправить вопрос в текущем виде?`,
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback('Дополнить', 'question-create-edit'),
                    Markup.button.callback('Отправить »', 'question-create-send'),
                ],
            ]),
            parse_mode: 'HTML',
        },
    )
}
