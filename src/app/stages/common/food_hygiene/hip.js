/**
 * Hip input
 */
import { Markup } from 'telegraf'
import { allScenes, onInput } from '#scene'
import { toHTML } from '#utils/entities.js'
import { hiddenTextLink } from '#utils/helpers.js'
import { activeDialog } from './active.js'

/** Dialog */
export async function hipDialog(ctx) {
    onInput(ctx, hipValue)

    return [
        '❓Введите обхват своих бедер (см).',
        Markup.inlineKeyboard([
            Markup.button.callback('Как измерить?', 'food-hygiene-hip-helper'),
        ]),
    ]
}

/** Actions */
allScenes('food-hygiene-hip-helper', async ctx => {
    if (! ctx.session._foodHygiene) {
        return ctx.editMessageText(
            'Сообщение уже устарело.\n' +
            'Выполните команду /food_hygiene чтобы начать новый тест.'
        )
    }

    const image_url = hiddenTextLink('https://doc.tyomka896.ru/katefit/body/female_hip.jpg')

    const text = toHTML(ctx.update.callback_query.message)

    return ctx.editMessageText(
        image_url + text +
        '\n\n<i>Обхват бедер измеряют горизонтально по наиболее выступающим точкам ягодиц:</i>',
        { parse_mode: 'HTML' }
    )
})

/** Messages */
export async function hipValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Обхват бедер не может быть пустым.')
    }

    value = parseFloat(value.replace(',', '.'))

    if (Number.isNaN(value)) {
        return ctx.reply('Обхват бедер может быть только числом.')
    }
    else if (value <= 25) {
        return ctx.reply(
            '<i>Обхват бедер не может быть настолько маленьким.</i>\n\n' +
            '❓Введите свой настоящий обхват бедер.',
            { parse_mode: 'HTML' }
        )
    }
    else if (value >= 240) {
        return ctx.reply(
            '<i>В мире многое бывает, но такие бедра это перебор.</i>\n\n' +
            '❓Введите свой настоящий обхват бедер.',
            { parse_mode: 'HTML' }
        )
    }

    ctx.session._foodHygiene.hip = value

    const [ text, extra ] = await activeDialog(ctx)

    return ctx.reply(
        `Вы ввели <u>${value} см. как обхват бедер</u>.\n\n${text}`,
        { ...extra, parse_mode: 'HTML' },
    )
}
