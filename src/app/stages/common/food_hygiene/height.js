/**
 * Height input
 */
import { onInput } from '#scene'
import { neckDialog } from './neck.js'

/** Dialog */
export async function heightDialog(ctx) {
    onInput(ctx, heightValue)

    return [ '❓Введите свой рост (см).' ]
}

/** Messages */
export async function heightValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Рост не может быть пустым.')
    }

    value = parseFloat(value.replace(',', '.'))

    if (Number.isNaN(value)) {
        return ctx.reply('Рост может быть только числом.')
    }
    else if (value <= 60) {
        return ctx.reply(
            '<i>Рост не может быть настолько маленьким.</i>\n\n' +
            '❓Введите свой настоящий рост.',
            { parse_mode: 'HTML' }
        )
    }
    else if (value >= 251) {
        return ctx.reply(
            '<i>Турок <b>Султан Кёсен</b> в настоящий момент ' +
            'является самым высоким человеком в мире, ' +
            'рост которого составляет 251 сантиметр.</i>\n\n' +
            'Сомневаюсь, что мне пишет именно он 😉\n' +
            '❓Введите свой настоящий рост.',
            { parse_mode: 'HTML' }
        )
    }

    ctx.session._foodHygiene.height = value

    const [ text, extra ] = await neckDialog(ctx)

    return ctx.reply(
        `Вы ввели <u>${value} см. как рост</u>.\n\n${text}`,
        { ...extra, parse_mode: 'HTML' },
    )
}
