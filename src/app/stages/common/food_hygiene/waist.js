/**
 * Waist input
 */
import { Markup } from 'telegraf'
import { allScenes, onInput } from '#scene'
import { toHTML } from '#utils/entities.js'
import { hiddenTextLink } from '#utils/helpers.js'
import { hipDialog } from './hip.js'
import { activeDialog } from './active.js'

/** Dialog */
export async function waistDialog(ctx) {
    onInput(ctx, waistValue)

    return [
        '❓Введите обхват своей талии (см).',
        Markup.inlineKeyboard([
            Markup.button.callback('Как измерить?', 'food-hygiene-waist-helper'),
        ]),
    ]
}

/** Actions */
allScenes('food-hygiene-waist-helper', async ctx => {
    if (! ctx.session._foodHygiene) {
        return ctx.editMessageText(
            'Сообщение уже устарело.\n' +
            'Выполните команду /food_hygiene чтобы начать новый тест.'
        )
    }

    let image_url = null

    if (ctx.session._foodHygiene.sex_woman) {
        image_url = hiddenTextLink('https://doc.tyomka896.ru/katefit/body/female_waist.jpg')
    }
    else {
        image_url = hiddenTextLink('https://doc.tyomka896.ru/katefit/body/male_waist.jpg')
    }

    const text = toHTML(ctx.update.callback_query.message)

    return ctx.editMessageText(
        image_url + text +
        '\n\n<i>Обхват талии измеряют по зафиксированной бельевой резинке:</i>',
        { parse_mode: 'HTML' }
    )
})

/** Messages */
export async function waistValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Обхват талии не может быть пустым.')
    }

    value = parseFloat(value.replace(',', '.'))

    if (Number.isNaN(value)) {
        return ctx.reply('Обхват талии может быть только числом.')
    }
    else if (value <= 25) {
        return ctx.reply(
            '<i>Обхват талии не может быть настолько маленьким.</i>\n\n' +
            '❓Введите свой настоящий обхват талии.',
            { parse_mode: 'HTML' }
        )
    }
    else if (value >= 300) {
        return ctx.reply(
            '<i>В мире многое бывает, но такая талия это перебор.</i>\n\n' +
            '❓Введите свой настоящий обхват талии.',
            { parse_mode: 'HTML' }
        )
    }

    ctx.session._foodHygiene.waist = value

    let text, extra

    if (ctx.session._foodHygiene.sex_woman) {
        ;[ text, extra ] = await hipDialog(ctx)
    }
    else {
        ctx.session._foodHygiene.hip = 0

        ;[ text, extra ] = await activeDialog(ctx)
    }

    return ctx.reply(
        `Вы ввели <u>${value} см. как обхват талии</u>.\n\n${text}`,
        { ...extra, parse_mode: 'HTML' },
    )
}
