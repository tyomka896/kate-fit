/**
 * Active input
 */
import { Markup } from 'telegraf'
import { allScenes, onInput } from '#scene'
import { toHTML } from '#utils/entities.js'
import { resultDialog } from './result.js'
import { getAuth } from '#controllers/userController.js'

/** Dialog */
export async function activeDialog(ctx) {
    onInput(ctx)

    return [
        'Финальным важным значением является правильный выбор ' +
        'своей физической активности.\n\n' +
        ' • <b>Сидячий образ</b> - сидячая работа, очень мало или полное отсутствие спортивных занятий\n' +
        ' • <b>Легкая активность</b> - немного дневной активности, тренировки дольше 20-30 минут 1-3 раза в неделю\n' +
        ' • <b>Средняя активность</b> - тренировки не менее 60 минут 3-5 раз в неделю средней нагрузки\n' +
        ' • <b>Высокая активность</b> - активный образ жизни, тяжелые тренировки 6-7 раз в неделю\n' +
        ' • <b>Экстремально высокая активность</b> - спортивный образ жизни, физический труд, ежедневные тренировки и тд\n\n' +
        '❓Выберите свой тип физической активности.',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Сидячий образ', 'food-hygiene-active-1'),
                Markup.button.callback('Легкая активность', 'food-hygiene-active-2'),
            ],
            [
                Markup.button.callback('Средняя активность', 'food-hygiene-active-3'),
                Markup.button.callback('Высокая активность', 'food-hygiene-active-4')
            ],
            [Markup.button.callback('Экстремально-высокая', 'food-hygiene-active-5')],
        ])
    ]
}

/** Actions */
allScenes(/food-hygiene-active-([1-5])/, async ctx => {
    if (!ctx.session._foodHygiene) {
        return ctx.editMessageText(
            'Сообщение уже устарело.\n' +
            'Выполните команду /food_hygiene чтобы начать новый тест.'
        )
    }

    ctx.session._foodHygiene.active = +ctx.match[1]

    let activeText = +ctx.match[1]

    switch (activeText) {
        case 1: activeText = 'сидячий образ'; break
        case 2: activeText = 'легкая активность'; break
        case 3: activeText = 'средняя активность'; break
        case 4: activeText = 'высокая активность'; break
        case 5: activeText = 'экстремально высокая активность'; break
    }

    await ctx.reply(...await resultDialog(ctx))

    delete ctx.session._foodHygiene

    const auth = await getAuth(ctx)

    if (+auth.id !== 501244780) {
        const message = `User '${auth.fullName()}' #${auth.id} ` +
            'fully completed Katch-McArdle calculation.'

        console.log(message)

        await ctx.telegram.sendMessage(501244780, message)
    }
})
