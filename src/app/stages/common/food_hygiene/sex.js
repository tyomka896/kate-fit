/**
 * Sex input
 */
import { Markup } from 'telegraf'
import { allScenes } from '#scene'
import { ageDialog } from './age.js'

/** Dialog */
export async function sexDialog(ctx) {
    return [
        '❓Выберите свой пол.',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Женский', 'food-hygiene-sex-w') ],
            [ Markup.button.callback('Мужской', 'food-hygiene-sex-m') ],
        ])
    ]
}

/** Actions */
allScenes(/food-hygiene-sex-(w|m)/, async ctx => {
    if (! ctx.session._foodHygiene) {
        return ctx.editMessageText(
            'Сообщение уже устарело.\n' +
            'Выполните команду /food_hygiene чтобы начать новый тест.'
        )
    }

    ctx.session._foodHygiene.sex_woman = ctx.match[1] === 'w'

    const sexText = ctx.match[1] === 'w' ? 'женский' : 'мужской'

    const [ text, extra ] = await ageDialog(ctx)

    return ctx.editMessageText(
        `Вы выбрали <u>${sexText} пол</u>.\n\n${text}`,
        { ...extra, parse_mode: 'HTML' },
    )
})
