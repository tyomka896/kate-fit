/**
 * Result of Katch-McArdle calculation
 */
import { onInput } from '#scene'
import { round, numberToWords } from '#utils/helpers.js'
import {
    dailyEnergyExpenditure,
    bodyFatText,
    expendituresText,
    obesityClassText,
} from '#utils/food_hygiene.js'

/** Dialog */
export async function resultDialog(ctx) {
    onInput(ctx)

    const { sex_woman, age } = ctx.session._foodHygiene

    const {
        body_mass_index,
        ideal_body_mass,
        body_fat_percentage,
        basal_metabolic_rate,
        resting_metabolic_rate,
        daily_energy_expenditure,
        daily_energy_expenditures,
    } = dailyEnergyExpenditure(ctx.session._foodHygiene)

    const expenditures = expendituresText(
        daily_energy_expenditure,
        daily_energy_expenditures,
    )

    const bodyFatClass = bodyFatText(sex_woman, age, body_fat_percentage)

    const obesityClasses = obesityClassText(body_mass_index)

    const ibm_min = Math.min(...ideal_body_mass)
    const ibm_max = Math.max(...ideal_body_mass)

    await sendInputInfo(ctx, ctx.session._foodHygiene)

    return [
        `💡 <b>Базальный/основной обмен:</b> ${round(basal_metabolic_rate, 0)} ккал/день\n` +
        `🛌 <b>Обмен во время отдыха:</b> ${round(resting_metabolic_rate, 0)} ккал/день\n\n` +
        (sex_woman ? '🏃‍♀️' : '🏃‍♂️') +
        ` <b>Расходы с учетом физ. активности:</b>\n` +
        expenditures + '\n' +
        bodyFatClass.emoji +
        ` <b>Процент жира:</b> ${round(body_fat_percentage, 2)}%\n` +
        '<b>Уровень процента жира:</b>\n' +
        bodyFatClass.text + '\n' +
        (sex_woman ? '🧍‍♀' : '🧍‍♂') +
        ` <b>Индекс массы тела:</b> ${round(body_mass_index, 2)} кг/м²\n` +
        '<b>Классификация массы тела по индексу:</b>\n' +
        obesityClasses + '\n' +
        (sex_woman ? '🧘‍♀️' : '🧘‍♂️') +
        ` <b>Идеальный вес:</b> ${round(ibm_min, 0)} - ${round(ibm_max, 0)} кг\n\n` +
        '👉 <i>Дополнительно вы можете ' +
        '<a href="https://t.me/' + env('BOT_NAME', 'BOT_NAME') +
        '?start=question">задать вопрос</a>' +
        ' тренеру по любому из пунктов</i>.',
        { parse_mode: 'HTML' },
    ]
}

async function sendInputInfo(ctx, info) {
    const { sex_woman, age, weight, height, neck, waist, hip, active } = info

    const activity = [
        'сидячий образ',
        'легкая',
        'средняя',
        'высокая',
        'экстремальная',
    ][active - 1]

    return ctx.editMessageText(
        '<b>Введены данные:</b>\n' +
        `• Пол: ${sex_woman ? 'женщина' : 'мужчина'}\n` +
        `• Возраст: ${age} ${numberToWords(age, 'год_года_лет')}\n` +
        `• Вес: ${weight} кг.\n` +
        `• Рост: ${height} cм.\n` +
        `• Шея: ${neck} см.\n` +
        `• Талия: ${waist} см.\n` +
        (sex_woman ? `• Бедра: ${hip} см.\n` : '') +
        `• Активность: ${activity}`,
        { parse_mode: 'HTML' },
    )
}
