/**
 * Neck input
 */
import { Markup } from 'telegraf'
import { allScenes, onInput } from '#scene'
import { toHTML } from '#utils/entities.js'
import { hiddenTextLink } from '#utils/helpers.js'
import { waistDialog } from './waist.js'

/** Dialog */
export async function neckDialog(ctx) {
    onInput(ctx, neckValue)

    return [
        '❓Введите обхват своей шеи (см).',
        Markup.inlineKeyboard([
            Markup.button.callback('Как измерить?', 'food-hygiene-neck-helper'),
        ])
    ]
}

/** Actions */
allScenes('food-hygiene-neck-helper', async ctx => {
    if (! ctx.session._foodHygiene) {
        return ctx.editMessageText(
            'Сообщение уже устарело.\n' +
            'Выполните команду /food_hygiene чтобы начать новый тест.'
        )
    }

    let image_url = null

    if (ctx.session._foodHygiene.sex_woman) {
        image_url = hiddenTextLink('https://doc.tyomka896.ru/katefit/body/female_neck.jpg')
    }
    else {
        image_url = hiddenTextLink('https://doc.tyomka896.ru/katefit/body/male_neck.jpg')
    }

    const text = toHTML(ctx.update.callback_query.message)

    return ctx.editMessageText(
        image_url + text +
        '\n\n<i>Обхват шеи измеряют по ее основанию:</i>',
        { parse_mode: 'HTML' }
    )
})

/** Messages */
export async function neckValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Обхват шеи не может быть пустым.')
    }

    value = parseFloat(value.replace(',', '.'))

    if (Number.isNaN(value)) {
        return ctx.reply('Обхват шеи может быть только числом.')
    }
    else if (value <= 15) {
        return ctx.reply(
            '<i>Обхват шеи не может быть настолько маленьким.</i>\n\n' +
            '❓Введите свой настоящий обхват шеи.',
            { parse_mode: 'HTML' }
        )
    }
    else if (value >= 55) {
        return ctx.reply(
            '<i>В мире многое бывает, но такая шея это перебор.</i>\n\n' +
            '❓Введите свой настоящий обхват шеи.',
            { parse_mode: 'HTML' }
        )
    }

    ctx.session._foodHygiene.neck = value

    const [ text, extra ] = await waistDialog(ctx)

    return ctx.reply(
        `Вы ввели <u>${value} см. как обхват шеи</u>.\n\n${text}`,
        { ...extra, parse_mode: 'HTML' },
    )
}
