/**
 * Weight input
 */
import { onInput } from '#scene'
import { heightDialog } from './height.js'

/** Dialog */
export async function weightDialog(ctx) {
    onInput(ctx, weightValue)

    return [ '❓Введите свой вес (кг).' ]
}

/** Messages */
export async function weightValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Вес не может быть пустым.')
    }

    value = parseFloat(value.replace(',', '.'))

    if (Number.isNaN(value)) {
        return ctx.reply('Вес может быть только числом.')
    }
    else if (value <= 2) {
        return ctx.reply(
            '<i>Вес не может быть настолько маленьким.</i>\n\n' +
            '❓Введите свой настоящий вес.',
            { parse_mode: 'HTML' }
        )
    }
    else if (value >= 645) {
        return ctx.reply(
            '<i>Американец <b>Джон Брауэр Миннок</b> является ' +
            'самым тяжелым подтвержденным мужчиной в истории медицины, ' +
            'вес которого оценивался около 645 килограмм.</i>\n\n' +
            'Сомневаюсь, что мне пишет именно он 😉\n' +
            '❓Введите свой настоящий вес.',
            { parse_mode: 'HTML' }
        )
    }

    ctx.session._foodHygiene.weight = value

    const [ text, extra ] = await heightDialog(ctx)

    return ctx.reply(
        `Вы ввели <u>${value} кг. как вес</u>.\n\n${text}`,
        { ...extra, parse_mode: 'HTML' },
    )
}
