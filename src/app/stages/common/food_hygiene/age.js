/**
 * Age input
 */
import { onInput } from '#scene'
import { weightDialog } from './weight.js'

/** Dialog */
export async function ageDialog(ctx) {
    onInput(ctx, ageValue)

    return [ '❓Введите свой возраст полных лет.' ]
}

/** Messages */
export async function ageValue(ctx) {
    let value = ctx.message.text

    if (! value) {
        return ctx.reply('Возраст не может быть пустым.')
    }

    value = parseInt(value)

    if (Number.isNaN(value)) {
        return ctx.reply('Возраст может быть только целым числом.')
    }
    else if (value <= 0) {
        return ctx.reply(
            '<i>Возраст не может быть отрицательным.</i>\n\n' +
            '❓Введите свой настоящий возраст.',
            { parse_mode: 'HTML' }
        )
    }
    else if (value >= 122) {
        return ctx.reply(
            '<i>Француженка <b>Жанна Луиза Кальман</b> является ' +
            'документально подтвержденной старейшей жительницей Земли, ' +
            'которая прожила 122 года 5 месяцев и 14 дней.</i>\n\n' +
            'Сомневаюсь, что мне пишет именно она 😉\n' +
            '❓Введите свой настоящий возраст.',
            { parse_mode: 'HTML' }
        )
    }

    ctx.session._foodHygiene.age = value

    const [ text, extra ] = await weightDialog(ctx)

    return ctx.reply(
        `Вы ввели <u>${value} как возраст</u>.\n\n${text}`,
        { ...extra, parse_mode: 'HTML' },
    )
}
