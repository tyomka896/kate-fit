/**
 * Katch-McArdle calculation
 */
import { Markup } from 'telegraf'
import { allScenes } from '#scene'
import { sexDialog } from './sex.js'
import { random } from '#utils/random.js'

/** Dialog */
export async function foodHygieneDialog(ctx) {
    return [
        random(['⛷', '🏂', '🪂', '🏋️‍♂️', '🤸‍♀️', '⛹️‍♀️', '🤾‍♂️', '🧘‍♀️', '🏄‍♂️', '🏊‍♀️', '🧗‍♂️']) +
        ' Для определения суточного количества калорий необходимо ' +
        'рассчитать показатель базального (основного) обмена. Это ' +
        'количество энергии, необходимое организму для поддержания ' +
        'жизненно важных функций без учëта энергозатрат на ' +
        'повседневную физическую активность (движение, умственная ' +
        'деятельность, тренировки и др.).\n\n' +
        random(['🥗', '🌽', '🍱', '🌮']) +
        ' На его основе в зависимости от твоей физической активности ' +
        'можно рассчитать примерную <b>итоговую суточную</b> норму ' +
        'калорий, необходимую для полного энергообеспечения организма.\n\n' +
        '👉 <i>Любые данные являются конфиденциальными и публично раскрываться не будут</i>.',
        {
            ...Markup.inlineKeyboard([
                [Markup.button.callback('Начать расчет', 'food-hygiene-start')],
            ]),
            parse_mode: 'HTML'
        }
    ]
}

/** Actions */
allScenes('food-hygiene-start', async ctx => {
    ctx.session._foodHygiene = {}

    return ctx.editMessageText(...await sexDialog(ctx))
})
