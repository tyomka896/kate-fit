/**
 * @KateFit certificates
 */
import sequelize from 'sequelize'
import { Markup } from 'telegraf'

import { allScenes } from '#scene'
import dayjs from '#utils/dayjs.js'
import { File } from '#models/file.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function certKeyboard(ctx) {
    const auth = await getAuth(ctx)
    const selectedYear = +ctx.session.certsYear

    let page = 0

    if (/(-view-page-|back-to-)/.test(ctx.match[0])) {
        if (ctx.match[1]) {
            page = +ctx.match[1]
        }
        else if (ctx.session.certsYearPage) {
            page = +ctx.session.certsYearPage
        }
    }

    const models = await File.findAll({
        where: {
            [sequelize.Op.and]: [
                { type: 'cert' },
                sequelize.where(
                    sequelize.fn('date_part', 'year', sequelize.col('created_at')),
                    selectedYear
                ),
            ]
        },
        order: ['created_at'],
    })

    const model = models[page]

    const modelUrl = model.url()

    const modelLink = certShareLink(model)

    return [
        model.link() +
        dayjs(model.created_at).format('DD.MM.YYYY г.'),
        {
            ...Markup.inlineKeyboard([
                [
                    Markup.button.callback(
                        '«',
                        `cert-view-page-${page - 1}`,
                        page === 0
                    ),
                    Markup.button.callback(
                        '»',
                        `cert-view-page-${page + 1}`,
                        models.length <= page + 1
                    ),
                ],
                [
                    Markup.button.url('Ссылка', modelUrl, !(modelUrl && auth.admin())),
                    Markup.button.url('Поделиться', modelLink, !auth.admin()),
                ],
                [Markup.button.callback('« Назад', 'back-to-certs-years')],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
allScenes(/cert-view-page-(\d+)/, async ctx => {
    ctx.session.certsYearPage = ctx.match[1]

    return ctx.editMessageText(...await certKeyboard(ctx))
})

/** Methods */

/**
 * Create share link
 */
function certShareLink(file) {
    const botLink =
        `https://t.me/${env('BOT_NAME', 'BOT_NAME')}?` +
        `start=cert_${file.id}`

    const createdAt = dayjs(file.created_at).format('DD.MM.YYYY')

    const shareText = `\nСертификат от ${createdAt} г.`

    return 'https://t.me/share/url?' +
        `url=${encodeURI(botLink)}&` +
        `text=${encodeURI(shareText)}`
}
