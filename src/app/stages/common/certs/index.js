/**
 * @KateFit certificates
 */
import sequelize from 'sequelize'
import { Markup } from 'telegraf'

import { allScenes } from '#scene'
import { File } from '#models/file.js'
import { certKeyboard } from './year.js'

/** Keyboard */
export async function certsKeyboard(ctx) {
    const years = await File.findAll({
        attributes: [
            [ sequelize.fn('date_part', 'year', sequelize.col('created_at')), 'year' ],
        ],
        group: 'year',
        order: [ 'year' ],
    })

    const yearButtons = years.map(elem => (
        [
            Markup.button.callback(
                elem.dataValues.year,
                `certs-year-${elem.dataValues.year}`
            )
        ]
    ))

    return [
        'Выберите год обучения',
        Markup.inlineKeyboard([ ...yearButtons ]),
    ]
}

/** Actions */
allScenes(/certs-year-(\d+)/, async ctx => {
    ctx.session.certsYear = ctx.match[1]

    return ctx.editMessageText(...await certKeyboard(ctx))
})

allScenes('back-to-certs-years', async ctx => {
    return ctx.editMessageText(...await certsKeyboard(ctx))
})
