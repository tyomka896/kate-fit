/**
 * Scenes definition
 */
import { adminScene, userScene } from './scene.js'

export function allScenes(actionName, method) {
    if (
        typeof actionName !== 'string' &&
        typeof actionName !== 'object'
    ) return

    if (typeof method !== 'function') return

    userScene.action(actionName, method)
    adminScene.action(actionName, method)
}

/**
 * User's input handler
 */
async function inputHandler(ctx) {
    if (!ctx.session.input) {
        return ctx.reply('Отправь /help для вывода полного списка команд.')
    }

    if (ctx.message.text === '/empty') {
        ctx.message.text = null
    }

    const { path, name } = ctx.session.input

    const module = await import(path)

    if (module[name]) {
        return module[name](ctx)
    }
}

userScene.hears(/^([^\/]|\/empty).*/, inputHandler)
adminScene.hears(/^([^\/]|\/empty).*/, inputHandler)


/**
 * Delete message action
 */
async function deleteMessage(ctx) {
    await ctx.answerCbQuery()

    await ctx.deleteMessage()
        .catch(() => {
            const { message_id } = ctx.update?.callback_query?.message

            const extra = message_id ?
                { reply_to_message_id: message_id } :
                null

            ctx.reply(
                'Не удалось удалить сообщение, удалите его вручную.',
                extra
            )
        })
}

allScenes('delete-message', deleteMessage)

export { adminScene, userScene }

export { isInput, onInput } from './hear.js'
