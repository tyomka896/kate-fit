/**
 * Main scene declaration
 */
import { Scenes } from 'telegraf'

export const adminScene = new Scenes.BaseScene('admin')

export const userScene = new Scenes.BaseScene('user')
