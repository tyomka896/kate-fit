/**
 * Main stage
 */
import { Scenes } from 'telegraf'

import { adminScene } from './admin/index.js'
import { userScene } from './user/index.js'

export default new Scenes.Stage([
    adminScene,
    userScene,
])
