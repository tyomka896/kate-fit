/**
 * Main stage
 */
import { userScene } from '#scene'
import help from '../../commands/help.js'
import { certsKeyboard } from '../common/certs/index.js'
import { foodHygieneDialog } from '../common/food_hygiene/index.js'
import { questionDialog } from '../common/question/index.js'
import { startKeyboard } from '../user/start/index.js'

/** Actions */
userScene.enter(async ctx => {
    switch (ctx.scene.state.act) {
        case 'food_hygiene': return ctx.reply(...await foodHygieneDialog(ctx))
        case 'question': return ctx.reply(...await questionDialog(ctx))
        case 'start': return ctx.reply(...await startKeyboard(ctx))
        case 'certs': return ctx.reply(...await certsKeyboard(ctx))
        default: return help(ctx)
    }
})

export { userScene }
