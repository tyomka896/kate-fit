/**
 * Week days selection
 */
import { Markup } from 'telegraf'
import { userScene } from '#scene'
import { Week } from '#models/week.js'
import { weekdayKeyboard } from './view.js'

/** Keyboard */
export async function weekKeyboard(ctx) {
    const models = (await Week.findAll())
        .reduce((red, elem) => {
            red[elem.id] = elem

            return red
        }, {})

    const weekButtons = [
        { key: 'MON', name: 'Понедельник' },
        { key: 'TU', name: 'Вторник' },
        { key: 'WED', name: 'Среда' },
        { key: 'TH', name: 'Четверг' },
        { key: 'FRI', name: 'Пятница' },
        { key: 'SAT', name: 'Суббота' },
    ]
        .reduce((red, elem) => {
            if (
                ! models[elem.key].workout_id ||
                ! models[elem.key].place ||
                ! models[elem.key].time
            ) {
                return red
            }

            red.push([
                Markup.button.callback(
                    `${elem.name}`,
                    `week-model-${elem.key}`
                )
            ])

            return red
        }, [])

    const header = weekButtons.length > 0 ?
        'Расписание на неделю' :
        'Эта неделя без тренировок'

    return [
        header,
        Markup.inlineKeyboard([
            ...weekButtons,
            [ Markup.button.callback('« Назад', 'back-to-start') ],
        ])
    ]
}

/** Actions */
userScene.action(/week-model-(MON|TU|WED|TH|FRI|SAT|SUN)/, async ctx => {
    ctx.session.weekId = ctx.match[1]

    return ctx.editMessageText(...await weekdayKeyboard(ctx))
})

userScene.action('back-to-week', async ctx => {
    return ctx.editMessageText(...await weekKeyboard(ctx))
})
