/**
 * Week day settings
 */
import { Markup } from 'telegraf'

import dayjs from '#utils/dayjs.js'
import { Week } from '#models/week.js'

/** Keyboard */
export async function weekdayKeyboard(ctx) {
    const week = await Week.findOne({
        where: { id: ctx.session.weekId }
    }) || await Week.create({ id: ctx.session.weekId })

    const workout = await week.getWorkout()

    const weekDayPrep = week.id === 'TU' ? 'Во' : 'В'

    const weekDayName = {
        'MON': 'Понедельник',
        'TU': 'Вторник',
        'WED': 'Среду',
        'TH': 'Четверг',
        'FRI': 'Пятницу',
        'SAT': 'Субботу',
        'SUN': 'Воскресенье',
    }[ctx.session.weekId]

    const startAt = ! week.time ? '🚫' : dayjs(week.time).tz().format('HH:mm')

    const rows = [
        `🗓 ${weekDayPrep} <b>${weekDayName}</b> ` +
        `состоится <u><i>${workout.name}</i></u>\n\n`,
        `⏱ <b>${startAt}</b>\n`,
        `📍 ${week.place}`,
    ]

    if (workout.about) {
        rows.push(`\n\n📝 ${workout.about}`)
    }

    if (workout.extra) {
        rows.push(`\n\n👉 <b>Вам потребуется:</b>\n${workout.extra}`)
    }

    return [
        rows.join(''),
        {
            ...Markup.inlineKeyboard([
                [ Markup.button.callback('« Назад', 'back-to-week') ],
            ]),
            parse_mode: 'HTML',
        }
    ]
}
