/**
 * Main start scene
 */
import { Markup } from 'telegraf'
import { userScene } from '#scene'
import { weekKeyboard } from './week/index.js'
import { workoutsKeyboard } from './workout/index.js'

/** Keyboard */
export async function startKeyboard(ctx) {
    return [
        'Главное меню',
        Markup.inlineKeyboard([
            [ Markup.button.callback('Расписание', 'start-week') ],
            [ Markup.button.callback('Тренировки', 'start-workouts') ],
        ])
    ]
}

/** Actions */
userScene.action('start-week', async ctx => {
    return ctx.editMessageText(...await weekKeyboard(ctx))
})

userScene.action('start-workouts', async ctx => {
    return ctx.editMessageText(...await workoutsKeyboard(ctx))
})

userScene.action('back-to-start', async ctx => {
    return ctx.editMessageText(...await startKeyboard(ctx))
})
