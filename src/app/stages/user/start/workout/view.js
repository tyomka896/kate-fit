/**
 * Workout view
 */
import { Sequelize, Op } from 'sequelize'
import { Markup } from 'telegraf'

import dayjs from '#utils/dayjs.js'
import { gender } from '#utils/emoji.js'
import { userScene } from '#scene'
import { User } from '#models/user.js'
import { Poll } from '#models/poll.js'
import { Week } from '#models/week.js'
import { Workout } from '#models/workout.js'
import { workoutsKeyboard } from './index.js'
import { getAuth } from '#controllers/userController.js'

/** Keyboard */
export async function workoutViewKeyboard(ctx) {
    const model = await Workout.findByPk(ctx.session.workoutId)

    const rows = [
        `${gender()} <b>${model.name}</b>`,
    ]

    if (model.about) {
        rows.push(`\n\n📝 ${model.about}`)
    }

    if (model.extra) {
        rows.push(`\n\n👉 <b>Вам потребуется:</b>\n${model.extra}`)
    }

    return [
        rows.join(''),
        {
            ...Markup.inlineKeyboard([
                [Markup.button.callback('Хочу посетить', 'workout-I-want-1')],
                [Markup.button.callback('« Назад', 'back-to-workouts')],
            ]),
            parse_mode: 'HTML',
        }
    ]
}

/** Actions */
userScene.action('workout-I-want-1', async ctx => {
    const workout = await Workout.findByPk(ctx.session.workoutId)

    if (!workout) {
        return ctx.editMessageText(...await workoutsKeyboard(ctx))
    }

    const week = await Week.findOne({
        where: {
            workout_id: workout.id,
            time: { [Op.not]: null },
            place: { [Op.not]: null },
            has_poll: 1,
        }
    })

    if (week) {
        let replyMessage = ''
        const keyboardButtons = [
            [
                Markup.button.callback(
                    '« Назад',
                    `workout-model-${workout.id}`
                )
            ],
        ]

        const weekDayIndex = {
            'MON': 1,
            'TU': 2,
            'WED': 3,
            'TH': 4,
            'FRI': 5,
            'SAT': 6,
            'SUN': 0,
        }[week.id]

        const weekDayPrep = week.id === 'TU' ? 'во' : 'в'

        const weekDayName = {
            'MON': 'Понедельник',
            'TU': 'Вторник',
            'WED': 'Среду',
            'TH': 'Четверг',
            'FRI': 'Пятницу',
            'SAT': 'Субботу',
            'SUN': 'Воскресенье',
        }[week.id]

        if (dayjs().day() === weekDayIndex) {
            replyMessage =
                `<u><i>${workout.name}</i></u> состоится уже сегодня.\n\n` +
                '❗️<i>Успевайте проголосовать в ' +
                `<a href="https://t.me/+_Uhq6BK1dpU5M2E6">основном чате</a>.</i>`

            const poll = await Poll.findOne({
                where: {
                    [Op.and]: [
                        Sequelize.where(
                            Sequelize.fn('date', Sequelize.col('end_at')),
                            dayjs().format('YYYY-MM-DD')
                        ),
                        { is_closed: 0 },
                    ]
                }
            })

            if (poll) {
                keyboardButtons.unshift(
                    [Markup.button.url('Перейти к опросу', poll.groupLink())]
                )
            }
        }
        else if (dayjs().day() > weekDayIndex) {
            replyMessage =
                `<u><i>${workout.name}</i></u> уже состоялась на этой неделе ` +
                `${weekDayPrep} <b>${weekDayName}</b>.\n\n` +
                '❗️<i>Ожидайте объявления нового расписания в ближайшее Воскресенье.</i>'
        }
        else {
            replyMessage =
                `<u><i>${workout.name}</i></u> состоится на этой неделе ` +
                `${weekDayPrep} <b>${weekDayName}</b>.\n\n` +
                '❗️<i>Объявление появится за день до начала тренировки.</i>'
        }

        return ctx.editMessageText(
            replyMessage,
            {
                ...Markup.inlineKeyboard([...keyboardButtons]),
                parse_mode: 'HTML',
                disable_web_page_preview: true,
            }
        )
    }

    return ctx.editMessageText(
        'В текущее расписание данная тренировка не включена.\n\n' +
        '❓Отправить сообщение, что вы хотите посетить эту тренировку?',
        Markup.inlineKeyboard([
            [
                Markup.button.callback('Нет', `workout-model-${workout.id}`),
                Markup.button.callback('Да', 'workout-I-want-2'),
            ],
        ])
    )
})

userScene.action('workout-I-want-2', async ctx => {
    const model = await Workout.findByPk(ctx.session.workoutId)

    if (!model) {
        return ctx.editMessageText(...await workoutsKeyboard(ctx))
    }

    const user = await getAuth(ctx)

    const message =
        `❗️Пользователь <i>${user.fullName()}</i> отметил, ` +
        `что хотел бы посетить тренировку <b>${model.name}</b>.`

    const admins = await User.findAll({ where: { role: 'admin' } })

    for (let admin of admins) {
        await ctx.telegram.sendMessage(
            admin.id,
            message,
            { parse_mode: 'HTML' }
        )
            .catch(() => { })
    }

    return ctx.editMessageText(
        'Пожелание успешно отправлено 🤸',
        Markup.inlineKeyboard([
            [Markup.button.callback('« Назад', `workout-model-${model.id}`)],
        ])
    )
})
