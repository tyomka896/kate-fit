/**
 * Training program for the next week
 */
import schedule from 'node-schedule'

import dayjs from '#utils/dayjs.js'
import { Chat } from '#models/chat.js'
import { createPoll } from '#controllers/pollController.js'
import { getActiveWeek, weekDescription } from '#controllers/weekController.js'

let _bot = null

/** Run the script at hour in Krasnoyarsk */
const HOUR_R24 = 15

export async function weekProgram() {
    // Temporary disable week description
    return

    const chat = await Chat.findOne({
        where: { active: 1 }
    })

    if (!chat) return

    const description = await weekDescription()

    if (!description) return

    try {
        await _bot.telegram.sendMessage(
            chat.id,
            description,
            { parse_mode: 'HTML' }
        )
    } catch (error) {
        return console.error(`Error to send week program - ${error.message}`)
    }

    const week = await getActiveWeek('MON')

    if (dayjs().day() === 0 && week) {
        createPoll(_bot, chat, week)
    }
}

export default async function (bot) {
    if (!bot) return null

    _bot = bot

    const cron = dayjs()
        .tz()
        .hour(HOUR_R24)
        .tz(dayjs.tz.guess())
        .format('0 H * * 0')

    return schedule.scheduleJob('program', cron, weekProgram)
}
