/**
 * Poll for groups
 */
import schedule from 'node-schedule'

import dayjs from '#utils/dayjs.js'
import { Chat } from '#models/chat.js'
import { Poll } from '#models/poll.js'
import {
    getActiveWeek,
    weekdayDescription,
} from '#controllers/weekController.js'
import {
    createPoll,
    createPollStopping,
    createPollReminder,
} from '#controllers/pollController.js'

let _bot = null

/** Run the script at hour in Krasnoyarsk */
const HOUR_R24 = 19

/**
 * Send workout description and the poll
 */
async function workoutPoll() {
    const chat = await Chat.findOne({
        where: { active: 1 }
    })

    if (!chat) {
        return
    }

    const weekDayId = {
        0: 'SUN',
        1: 'MON',
        2: 'TU',
        3: 'WED',
        4: 'TH',
        5: 'FRI',
        6: 'SAT',
    }[dayjs().add(1, 'day').day()]

    const week = await getActiveWeek(weekDayId)

    if (!week) {
        return
    }

    try {
        await _bot.telegram.sendMessage(
            chat.id,
            await weekdayDescription(week),
            { parse_mode: 'HTML' }
        )
    } catch (error) {
        return console.error(
            `Error to send weekday description ` +
            `to chat #${chat.id} - ${error.message}`
        )
    }

    await createPoll(_bot, chat, week)
}

export default async function (bot) {
    if (!bot) return null

    _bot = bot

    const polls = await Poll.findAll({ where: { is_closed: 0 } })

    for (let poll of polls) {
        await createPollStopping(_bot, poll)
        await createPollReminder(_bot, poll)
    }

    const cron = dayjs()
        .tz()
        .hour(HOUR_R24)
        .tz(dayjs.tz.guess())
        .format('0 H * * 1-7')

    return schedule.scheduleJob('voting', cron, workoutPoll)
}
