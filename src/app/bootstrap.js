/**
 * Bootstrapping the Telegram bot
 */
import './http/connection.js'
import mainStage from './stages/index.js'

/** Middleware */
const middleware = [
    'event',
    'shutDown',
    'signIn',
    'chatIn',
    'session',
    'lastVisit',
]

/** Commands */
const commands =[
    'bonuses',
    'bonus',
    'cancel',
    'certs',
    'dump',
    'food_hygiene',
    'help',
    'links',
    'payment',
    'program',
    'question',
    'questions',
    'role',
    'settings',
    'start',
    'status',
    'ticket',
]

/**
 * Other features
 */
const others = [
    'poll',
]

/**
 * Schedules
 */
const schedules = [
    'voting',
    'program',
]

/**
 * Bootstrapping the bot
 */
export async function Bootstrap(bot) {
    /** Setting basic middleware */
    for (let _middleware of middleware) {
        const module = await import(`./http/middleware/${_middleware}.js`)

        bot.use(module.default)
    }

    /** Setting main stage */
    bot.use(mainStage.middleware())

    /** Other features */
    for (let other of others) {
        const module = await import(`./other/${other}.js`)

        module.default(bot)
    }

    /** Setting commands to bot */
    for (let command of commands) {
        const module = await import(`./commands/${command}.js`)

        bot.command(command, module.default)
    }

    bot.hears(/^\/.*/, ctx =>
        ctx.reply(
            'Не известная команда.\n' +
            'Отправь /help для вывода полного списка команд.'
        )
    )

    /** Setting schedules */
    for (let schedule of schedules) {
        const module = await import(`./schedule/${schedule}.js`)

        module.default(bot)
    }

    bot.catch(error => console.error(error))

    return bot
}
