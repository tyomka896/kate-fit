/**
 * Bot usage logging
 */
import { Event } from '#models/event.js'

export default async function(ctx, next) {
    next()

    if (! ctx.update || ! ctx.chat?.type) return

    const { message, callback_query } = ctx.update

    const eventKeys = {
        event_id: 'message',
        type_id: 'message',
        chat_type: ctx.chat.type,
    }

    if (message?.text && message.text.slice(0, 1) === '/') {
        eventKeys.event_id = message.text.slice(1)
            .replace('@' + env('BOT_NAME'), '')
        eventKeys.type_id = 'command'
    }
    else if (callback_query?.data) {
        if (
            /back-to.*/.test(callback_query.data) ||
            /(.*)--?\d+/.test(callback_query.data)
        ) return

        eventKeys.event_id = callback_query.data
        eventKeys.type_id = 'callback_query'
    }
    else return

    let event = await Event.findOne({ where: eventKeys })

    event ? await event.increment({ count: 1 }) :
        await Event.create({ ...eventKeys, count: 1 })
}
