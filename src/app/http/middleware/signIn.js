/**
 * Sign in the user
 */
import { getAuth, createUser } from '#controllers/userController.js'

export default async function(ctx, next) {
    if (! ctx.from || ctx.from.is_bot) return next()

    const auth = await getAuth(ctx) || await createUser({
        id: ctx.from.id,
        username: ctx.from.username,
        first_name: ctx.from.first_name,
        last_name: ctx.from.last_name,
    })

    if (auth.username != ctx.from.username) {
        await auth.update({
            username: ctx.from.username || null,
        })
    }

    return next()
}
