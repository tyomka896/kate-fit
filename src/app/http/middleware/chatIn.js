/**
 * Create the chat
 */
import { getChat, createChat } from '#controllers/chatController.js'

export default async function(ctx, next) {
    if (! ctx.chat || ctx.chat.type === 'private') return next()

    const chat = await getChat(ctx) || await createChat({
        id: ctx.chat.id,
        name: ctx.chat.title,
    })

    if (chat.name !== ctx.chat.title) {
        await chat.update({
            name: ctx.chat.title,
        })
    }

    return next()
}
