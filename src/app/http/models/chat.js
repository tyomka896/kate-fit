/**
 * Chat model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/**
 * Extending model
 */
export class Chat extends Model {  }

/**
 * Model structure
 */
Chat.init({
    id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    active: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    name: {
        type: DataTypes.STRING(150),
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'chat',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})
