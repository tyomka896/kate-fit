/**
 * File model
 */
import dayjs from '#utils/dayjs.js'
import { Model, DataTypes } from 'sequelize'

import { sequelize } from '#connection'
import { hiddenTextLink } from '#utils/helpers.js'

/**
 * Extending model
 */
export class File extends Model {
    url() {
        if (! env('DOC_URL') || ! env('DOC_PATH')) return ''

        const _path = this.path.replace(env('DOC_PATH'), '')

        return env('DOC_URL') + _path
    }

    link() {
        if (! env('DOC_URL') || ! env('DOC_PATH')) return ''

        return hiddenTextLink(this.url())
    }
}

/**
 * Model structure
 */
File.init({
    id: {
        type: DataTypes.STRING(10),
        primaryKey: true,
    },
    type: {
        type: DataTypes.STRING(10),
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING(10),
        allowNull: false,
    },
    size: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    path: {
        type: DataTypes.STRING(260),
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'file',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})
