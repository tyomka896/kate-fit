/**
 * Week model
 */
import { Model, DataTypes } from 'sequelize'

import { sequelize } from '#connection'
import { toHTML } from '#utils/entities.js'
import { Workout } from './workout.js'

/**
 * Extending model
 */
export class Week extends Model { }

/**
 * Model structure
 */
Week.init({
    id: {
        type: DataTypes.STRING(10),
        primaryKey: true,
    },
    workout_id: {
        type: DataTypes.INTEGER,
    },
    time: {
        type: DataTypes.DATE,
    },
    place: {
        type: DataTypes.STRING(200),
        set(value) {
            this.setDataValue('place', toHTML(value))
        },
    },
    has_poll: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 1,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'week',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})

/**
 * Get the workout that owns the week.
 */
Week.belongsTo(Workout, {
    foreignKey: 'workout_id',
    as: 'workout',
})

/**
 * Get the weeks for the workout.
 */
Workout.hasMany(Week, {
    foreignKey: 'workout_id',
    as: 'weeks',
})
