/**
 * Workout model
 */
import { Model, DataTypes } from 'sequelize'

import { sequelize } from '#connection'
import { escapeHTML, toHTML } from '#utils/entities.js'

/**
 * Extending model
 */
export class Workout extends Model {  }

/**
 * Model structure
 */
Workout.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    user_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING(250),
        allowNull: false,
        set(value) {
            this.setDataValue('name', escapeHTML(value))
        },
    },
    short_name: {
        type: DataTypes.STRING(50),
        allowNull: false,
        set(value) {
            this.setDataValue('short_name', escapeHTML(value))
        },
    },
    about: {
        type: DataTypes.TEXT,
        set(value) {
            this.setDataValue('about', toHTML(value))
        },
    },
    extra: {
        type: DataTypes.TEXT,
        set(value) {
            this.setDataValue('extra', toHTML(value))
        },
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'workout',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})
