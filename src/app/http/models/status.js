/**
 * Status model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'
import { User } from '#models/user.js'
import { Poll } from '#models/poll.js'

/**
 * Extending model
 */
export class Status extends Model {  }

/**
 * Model structure
 */
Status.init({
    user_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    message_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    poll_id: {
        type: DataTypes.STRING(20),
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'status',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: false,
})

/**
 * Get the user that owns the status.
 */
Status.belongsTo(User, {
    foreignKey: 'user_id',
    as: 'user',
})

/**
 * Get the statuses for the user.
 */
User.hasMany(Status, {
    foreignKey: 'user_id',
    as: 'statuses',
})

/**
 * Get the poll that owns the status.
 */
Status.belongsTo(Poll, {
    foreignKey: 'poll_id',
    as: 'poll',
})

/**
 * Get the statuses for the poll.
 */
Poll.hasMany(Status, {
    foreignKey: 'poll_id',
    as: 'statuses',
})
