/**
 * UserHasPoll model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/**
 * Extending model
 */
export class UserHasPoll extends Model {
    /**
     * Update answer integrity
     */
    updateIntegrity() {
        const answer = JSON.parse(this.values)[0]
        const wasThere = Boolean(this.payment || this.has_bonus)

        if (answer === 0) {
            this.integrity = wasThere ? 1 : 0
        }
        else if (answer === 1) {
            this.integrity = .5
        }
        else if (answer === 2) {
            this.integrity = wasThere ? 0 : 1
        }
        else if (answer === undefined) {
            this.integrity = ! this.payment || this.has_bonus ? 1 : 0
        }

        return this
    }
}

/**
 * Model structure
 */
UserHasPoll.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    user_id: {
        type: DataTypes.BIGINT,
    },
    poll_id: {
        type: DataTypes.STRING(20),
        allowNull: false,
    },
    values: {
        type: DataTypes.STRING(50),
    },
    payment: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    has_bonus: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    integrity: {
        type: DataTypes.FLOAT,
        allowNull: false,
        defaultValue: 1,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'user_has_poll',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})
