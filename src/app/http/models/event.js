/**
 * Event model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'

/**
 * Extending model
 */
export class Event extends Model {  }

/**
 * Model structure
 */
Event.init({
    event_id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    type_id: {
        type: DataTypes.STRING(15),
        primaryKey: true,
    },
    chat_type: {
        type: DataTypes.STRING(15),
        primaryKey: true,
    },
    count: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'event',
    timestamps: true,
    createdAt: false,
    updatedAt: 'updated_at',
})
