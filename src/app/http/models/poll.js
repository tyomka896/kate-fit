/**
 * Poll model
 */
import { Model, DataTypes } from 'sequelize'
import { Chat } from './chat.js'
import { Week } from './week.js'
import { UserHasPoll } from './user_has_poll.js'
import { sequelize } from '#connection'

/**
 * Extending model
 */
export class Poll extends Model {
    /**
     * Link to the poll in group chat
     */
    groupLink() {
        return 'https://t.me/c/' +
            this.chat_id.toString().slice(4) + '/' +
            this.message_id
    }
}

/**
 * Model structure
 */
Poll.init({
    id: {
        type: DataTypes.STRING(20),
        primaryKey: true,
    },
    chat_id: {
        type: DataTypes.BIGINT,
        allowNull: false,
    },
    week_id: {
        type: DataTypes.STRING(10),
        allowNull: false,
    },
    message_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    options: {
        type: DataTypes.STRING(300),
        allowNull: false,
    },
    total_voters: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    is_closed: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    is_reminded: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    is_pinned: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    total_payment: {
        type: DataTypes.SMALLINT,
        allowNull: false,
        defaultValue: 0,
    },
    end_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'poll',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})

/**
 * Get the chat that owns the poll.
 */
Poll.belongsTo(Chat, {
    foreignKey: 'chat_id',
    as: 'chat',
})

/**
 * Get the polls for the chat.
 */
Chat.hasMany(Poll, {
    foreignKey: 'chat_id',
    as: 'polls',
})

/**
 * Get the week that owns the poll.
 */
Poll.belongsTo(Week, {
    foreignKey: 'week_id',
    as: 'week',
})

/**
 * Get the polls for the week.
 */
Week.hasMany(Poll, {
    foreignKey: 'week_id',
    as: 'polls',
})

/**
 * Get the user_has_poll for the poll.
 */
Poll.hasMany(UserHasPoll, {
    foreignKey: 'poll_id',
})

/**
 * Get the poll for the user_has_poll.
 */
UserHasPoll.belongsTo(Poll, {
    foreignKey: 'poll_id'
})
