/**
 * User setting model
 */
import { Model, DataTypes } from 'sequelize'
import { sequelize } from '#connection'
import { UserSettingType } from './user_setting_type.js'

/**
 * Extending model
 */
export class UserSetting extends Model {  }

/**
 * Model structure
 */
UserSetting.init({
    user_id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
    },
    type_id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    value: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    created_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize,
    tableName: 'user_setting',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
})

/**
 * Get the UserSettingType that owns the UserSetting.
 */
UserSetting.belongsTo(UserSettingType, {
    foreignKey: 'type_id',
    as: 'type',
})

/**
 * Get the UserSettings for the UserSettingType.
 */
UserSettingType.hasMany(UserSetting, {
    foreignKey: 'type_id',
    as: 'setting',
})
