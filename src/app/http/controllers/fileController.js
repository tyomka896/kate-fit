/**
 * File controller
 */
import dayjs from '#utils/dayjs.js'
import { File } from '#models/file.js'

export async function shareFile(ctx, file) {
    if (
        ctx &&
        ctx.constructor?.name !== 'Telegraf' &&
        ctx.constructor?.name !== 'Context'
    ) {
        throw Error('Invalid parameter: ctx.')
    }

    if (typeof file === 'string') {
        file = await File.findByPk(file)
    }
    if (file?.constructor?.name !== 'File') {
        throw Error('Invalid parameter: file.')
    }

    return ctx.reply(
        file.link() +
        dayjs(file.created_at).format('DD.MM.YYYY') + ' г.',
        { parse_mode: 'HTML' },
    )
}
