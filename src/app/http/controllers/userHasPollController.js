/**
 * User has poll controller
 */
import { Sequelize, Op } from 'sequelize'

import dayjs from '#utils/dayjs.js'
import { numberToWords } from '#utils/helpers.js'
import { Poll } from '#models/poll.js'
import { User } from '#models/user.js'
import { UserHasPoll } from '#models/user_has_poll.js'
import { GlobalSetting } from '#models/global_setting.js'

export async function createOrUpdateUserHasPoll(user, poll, values) {
    if (typeof user === 'number') {
        user = await User.findByPk(user)
    }
    if (user?.constructor?.name !== 'User') {
        throw Error('Invalid parameter: user.')
    }

    if (typeof poll === 'number') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    if (!Array.isArray(values)) {
        throw Error('Invalid parameter: values.')
    }

    const userPollIds = { user_id: user.id, poll_id: poll.id }

    const pollPayment = (await GlobalSetting.findByPk('poll_payment')).value

    const payment = values[0] === 0 ? pollPayment : 0
    const integrity = values[0] === 1 ? 0.5 : 1.0

    if (await user.hasPoll(poll)) {
        await UserHasPoll.update({
            payment,
            integrity,
            values: JSON.stringify(values),
        }, { where: userPollIds })
    }
    else {
        await UserHasPoll.create({
            payment,
            integrity,
            ...userPollIds,
            values: JSON.stringify(values),
        })
    }
}

/**
 * All users with nearby bonus
 * @returns Array of users with last visit before bonus
 * or users that should be marked with one
 */
export async function usersWithBonus() {
    const BONUS_PER_VISITS = 8

    const startOfMonth = dayjs().utc().startOf('month')

    let bonuses = await UserHasPoll.findAll({
        attributes: [
            'user_id',
            [Sequelize.fn('COUNT', Sequelize.col('user_id')), 'count']
        ],
        where: {
            has_bonus: 1,
            created_at: { [Op.gte]: startOfMonth.format() },
        },
        group: ['user_id'],
    })

    bonuses = bonuses.reduce((red, elem) => {
        red[elem.user_id] = elem.toJSON().count

        return red
    }, {})

    const visits = await UserHasPoll.findAll({
        attributes: [
            'user_id',
            [Sequelize.fn('COUNT', Sequelize.col('user_id')), 'count']
        ],
        include: [{
            model: Poll,
            attributes: [],
            where: {
                is_closed: 1,
                end_at: { [Op.gte]: startOfMonth.format() }
            }
        }],
        where: {
            [Op.or]: [
                { payment: { [Op.gt]: 0 } },
                { has_bonus: 1 },
            ],
        },
        group: ['user_id'],
    })

    return visits.reduce((red, elem) => {
        const stat = {}

        stat.user_id = elem.user_id
        stat.visits = +elem.toJSON().count
        stat.bonuses = +bonuses[elem.user_id] || 0

        if (Math.floor(stat.visits / BONUS_PER_VISITS) > stat.bonuses) {
            stat.status = 'check'
        }
        else if (BONUS_PER_VISITS - (stat.visits % BONUS_PER_VISITS + 1) === 0) {
            stat.status = 'last'
        }

        red.push(stat)

        return red
    }, [])
}

/**
 * All users with nearby bonus as text
 * @returns Message with html format
 */
export async function usersWithBonusAsText() {
    const withBonuses = await usersWithBonus()

    if (withBonuses.length === 0) {
        return null
    }

    const bonusesStat = withBonuses.reduce((red, elem) => {
        red[elem.user_id] = elem

        return red
    }, {})

    const users = await User.findAll({
        where: { id: { [Op.in]: Object.keys(bonusesStat) } }
    })

    const checkRows = []
    const lastRows = []

    for (let user of users) {
        const visits = bonusesStat[user.id].visits

        const word = numberToWords(visits, 'треня_трени_трень')

        const text = `• ${user.fullName(false)} ` +
            `<i>(${bonusesStat[user.id].visits} ${word})</i>\n`

        if (bonusesStat[user.id].status === 'check') {
            checkRows.push(text)
        }
        else if (bonusesStat[user.id].status === 'last') {
            lastRows.push(text)
        }
    }

    const rows = []

    if (checkRows.length > 0) {
        rows.push('🔥 <b><i>Необходимо засчитать бонус</i></b>:\n')
        rows.push(checkRows.join(''))
    }

    if (lastRows.length > 0) {
        rows.push('\n⭐️ <i>Следующая треня бонусная</i>:\n')
        rows.push(lastRows.join(''))
    }

    return rows.join('').trim()
}
