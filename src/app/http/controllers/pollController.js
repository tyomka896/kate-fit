/**
 * Poll controller
 */
import { Markup } from 'telegraf'
import schedule from 'node-schedule'

import dayjs from '#utils/dayjs.js'
import { random } from '#utils/random.js'
import * as emoji from '#utils/emoji.js'
import { numberToWords } from '#utils/helpers.js'

import { Poll } from '#models/poll.js'
import { Chat } from '#models/chat.js'
import { Week } from '#models/week.js'
import { User } from '#models/user.js'
import { Status } from '#models/status.js'
import { UserHasPoll } from '#models/user_has_poll.js'
import { SETTING_TYPE } from '#models/user_setting_type.js'
import { getUserSettingValue } from '#controllers/userSettingController.js'

/** Default poll answers */
export const DEFAULT_ANSWERS = [
    'Приду 💪',
    '50/50 🤔',
    'Не приду 🙃',
]

/**
 * Randomize new poll question and its options
 * @returns {array}
 */
export function createRandomPoll() {
    /** Wordbook of the poll */
    const QUESTIONS = [
        'Придёте на тренировку❓',
        'Прошу проголосовать 🤗',
        'Поучаствуйте в опросе 🙏',
    ]

    const FIRST = [
        'Буду 100% 💪',
        'Приду, конечно 🤘',
        'Буду без вопросов 😁',
        'Обязательно приду 🖖',
        'Приду, не обсуждается 😜',
    ]

    const SECOND = [
        '50/50 🙄',
        'Хочу, но 50/50 🥹',
        'Постараюсь быть 🤔',
        'По возможности приду 🧐',
        'Приду, если получится 🤪',
    ]

    const THIRD = [
        'Пропущу 🫢',
        'Не приду 🫣',
        'Не смогу 🫠',
        'Меня не будет 🙃',
        'Ой-ой, только не я 😬',
    ]

    const question = random(QUESTIONS)

    const options = [
        random(FIRST),
        random(SECOND),
        random(THIRD),
    ]

    return { question, options }
}

/**
 * Create poll
 * @param {Telegraf|Context} bot
 * @param {Chat|number} chat
 * @param {Week|number} week
 * @returns
 */
export async function createPoll(bot, chat, week) {
    if (
        bot &&
        bot.constructor?.name !== 'Telegraf' &&
        bot.constructor?.name !== 'Context'
    ) {
        throw Error('Invalid parameter: bot.')
    }

    if (typeof chat === 'number') {
        chat = await Chat.findByPk(chat)
    }
    if (chat?.constructor?.name !== 'Chat') {
        throw Error('Invalid parameter: chat.')
    }

    if (typeof week === 'number') {
        week = await Week.findByPk(week)
    }
    if (week?.constructor?.name !== 'Week') {
        throw Error('Invalid parameter: week.')
    }

    const _randomPoll = createRandomPoll()

    const weekDayName = {
        'MON': 'ПОНЕДЕЛЬНИК',
        'TU': 'ВТОРНИК',
        'WED': 'СРЕДА',
        'TH': 'ЧЕТВЕРГ',
        'FRI': 'ПЯТНИЦА',
        'SAT': 'СУББОТА',
        'SUN': 'ВОСКРЕСЕНЬЕ',
    }[week.id]

    const startAt = dayjs(week.time)

    const pollInfo = await bot.telegram.sendPoll(
        chat.id,
        `${weekDayName} ⏱ ` +
        startAt.tz().format('HH:mm') +
        `\n\n${_randomPoll.question}`,
        _randomPoll.options,
        { is_anonymous: false }
    )

    /** End of the poll based on week time */
    const endAt = dayjs()
        .add(1, 'day')
        .hour(startAt.hour())
        .minute(startAt.minute())

    const poll = await Poll.create({
        id: pollInfo.poll.id,
        chat_id: chat.id,
        week_id: week.id,
        message_id: pollInfo.message_id,
        options: JSON.stringify(pollInfo.poll.options),
        total_voters: pollInfo.total_voter_count,
        end_at: endAt,
    })

    await sendAdminsPollStatus(bot, poll)
    await createPollStopping(bot, poll)
    await createPollReminder(bot, poll)

    console.log(`Poll #${poll.id} created.`)

    return poll
}

/**
 * Status of the poll schedule
 * @param {Telegraf|Context} bot
 * @param {Poll|string} poll
 * @returns string
 */
export async function sendAdminsPollStatus(bot, poll) {
    if (
        bot &&
        bot.constructor?.name !== 'Telegraf' &&
        bot.constructor?.name !== 'Context'
    ) {
        throw Error('Invalid parameter: bot.')
    }

    if (typeof poll === 'string') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    const admins = await User.findAll({ where: { role: 'admin' } })

    if (!admins.length) return null

    const currentStatus = await getStatusPoll(poll)

    const markup = Markup.inlineKeyboard([
        [
            Markup.button.callback(
                'К опросу »',
                `poll-status-go-to-${poll.id}`
            ),
        ],
    ])

    for (let admin of admins) {
        const sendStatus = await admin.getItemValue(SETTING_TYPE.STATUS, true)

        if (! +sendStatus) continue

        try {
            const data = await bot.telegram.sendMessage(
                admin.id,
                currentStatus,
                { ...markup, parse_mode: 'HTML' }
            )

            await Status.create({
                user_id: admin.id,
                message_id: data.message_id,
                poll_id: poll.id,
            })
        } catch (error) {
            console.error(
                `Error to send status of the poll #${poll.id} ` +
                `to user #${admin.id} - ${error.message}.`
            )
        }
    }
}

/**
 * Reminder of the active poll
 * @param {Telegraf|Context} bot
 * @param {Poll|string} poll
 * @returns
 */
export async function createPollReminder(bot, poll) {
    if (
        bot &&
        bot.constructor?.name !== 'Telegraf' &&
        bot.constructor?.name !== 'Context'
    ) {
        throw Error('Invalid parameter: bot.')
    }

    if (typeof poll === 'string') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    const cron = dayjs(poll.end_at)
        .subtract(1, 'hour')
        .subtract(30, 'minutes')

    if (dayjs().diff(cron) > 0) return null

    const statuses = await Status.findAll({ where: { poll_id: poll.id } })

    if (!statuses.length) return null

    const scheduleName = `poll-reminder-${poll.id}`

    const REMEMBER = random([
        'Проверь результаты опроса 🦦',
        'Хороший день для тренировки 🤸‍♀️',
        'Проходила мимо, решила напомнить 🏃‍♀️',
        'Не забудь, пожалуйста, про опрос 🐣',
        'Напоминаю, что сегодня тренировка 🧘‍♀️',
        'Тренировка - замечательная часть дня 💃',
    ])

    const THANKS = [
        'Удружила 🫶',
        'Благодарю 🙏',
        'Очень ценю 🐥',
        'Как вовремя 💐',
        'Вельми понеже 🙇‍♀️',
        'Ты просто чудо 😻',
    ]

    const markup = Markup.inlineKeyboard([
        [Markup.button.callback(random(THANKS), 'delete-message')],
    ])

    schedule.scheduleJob(
        scheduleName,
        cron.format('0 m H D M *'),
        async () => {
            for (let status of statuses) {
                const sendRemind = await getUserSettingValue(
                    status.user_id,
                    SETTING_TYPE.REMIND,
                    true
                )

                if (! +sendRemind) continue

                await bot.telegram.sendMessage(
                    status.user_id,
                    REMEMBER,
                    {
                        ...markup,
                        parse_mode: 'HTML',
                        reply_to_message_id: status.message_id,
                    }
                )
                    .catch(error =>
                        console.error(
                            `Error to send reminder of the poll #${poll.id} ` +
                            `to user #${admin.id} - ${error.message}.`
                        )
                    )
            }

            if (schedule.cancelJob(scheduleName)) {
                console.log(`Schedule ${scheduleName} closed.`)
            }
        })

    return true
}

/**
 * Get status of the poll
 * @param {Poll|string} poll
 * @returns string
 */
export async function getStatusPoll(poll) {
    if (typeof poll === 'string') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    const pollHasEnded = dayjs().diff(dayjs(poll.end_at)) > 0

    const users = await poll.getUsers({ order: ['updated_at'] })

    const noUsers = await UserHasPoll.findAll({
        where: { user_id: null, poll_id: poll.id },
    })

    const weekDayName = {
        0: 'Воскресенье',
        1: 'Понедельник',
        2: 'Вторник',
        3: 'Среду',
        4: 'Четверг',
        5: 'Пятницу',
        6: 'Субботу',
    }[dayjs(poll.end_at).tz().day()]

    const rows = [
        `${emoji.gender()} Опрос на <b>${weekDayName}</b>\n\n`,
    ]

    let totalVotes = 0
    const pollOthers = []
    const pollOptions = JSON.parse(poll.options)

    for (let user of users) {
        const { values, payment, has_bonus } = user.UserHasPoll

        let userInfo =
            (payment ? '✓ ' : (has_bonus ? '✩ ' : '• ')) +
            user.fullName(false)

        /** Show user integrity when poll open or payment when ended */
        if (!pollHasEnded) {
            userInfo += ` <i>[${user.integrity.toFixed(1)}]</i>`
        }
        else if (pollHasEnded && payment !== 0) {
            userInfo += ` - <i>${payment} ₽</i>`
        }

        const userValue = JSON.parse(values)[0]

        /** User has not voted, but was added to the poll afterwards */
        if (userValue === undefined) {
            if (payment || has_bonus) {
                pollOthers.push(userInfo)
            }

            continue
        }

        totalVotes += 1
        pollOptions[userValue].users ??= []
        pollOptions[userValue].users.push(userInfo)
    }

    const optionsResult = pollOptions.reduce((red, elem, index) => {
        if (!elem.users?.length) return red

        const rate = Math.round(elem.users.length / totalVotes * 100)

        const count =
            elem.users.length + ' ' +
            numberToWords(elem.users.length, 'голос_голоса_голосов')

        red.push(
            `<i>${DEFAULT_ANSWERS[index]}</i> - ` +
            `<b>${rate}%</b> <i>(${count})</i>\n` +
            elem.users.join('\n')
        )

        return red
    }, [])

    if (pollOthers.length) {
        const count = pollOthers.length + ' ' +
            numberToWords(pollOthers.length, 'человек_человека_человек')

        optionsResult.push(
            `<i>Другие</i> <i>(${count})</i>\n` +
            pollOthers.join('\n')
        )
    }

    if (!optionsResult.length) {
        rows.push('Никто не проголосовал 🤷‍♂️')
    }
    else {
        rows.push(optionsResult.join('\n\n'))

        if (noUsers.length) {
            const noUsersPayment =
                noUsers.reduce((red, elem) => red + elem.payment, 0)
            const noUsersWord =
                numberToWords(noUsers.length, 'человек_человека_человек')

            rows.push(
                `\n\n❓<i>+${noUsers.length} ${noUsersWord}` +
                ` - ${noUsersPayment} ₽</i>`
            )
        }

        if (pollHasEnded) {
            rows.push(`\n\n<i><u>Доход</u>: ${poll.total_payment} ₽</i>`)
        }
    }

    return rows.join('')
}

/**
 * Stop the active poll schedule
 * @param {Telegraf|Context} bot
 * @param {Poll|string} poll
 */
export async function createPollStopping(bot, poll) {
    if (
        bot &&
        bot.constructor?.name !== 'Telegraf' &&
        bot.constructor?.name !== 'Context'
    ) {
        throw Error('Invalid parameter: bot.')
    }

    if (typeof poll === 'string') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    /** Close the poll five minutes in advance */
    const stopAt = dayjs(poll.end_at).subtract(5, 'minutes')

    const cron = dayjs().diff(stopAt) < 0 ? stopAt : dayjs().add(3, 'seconds')

    const scheduleName = `poll-stop-${poll.id}`

    schedule.scheduleJob(
        scheduleName,
        cron.format('s m H D M *'),
        async () => {
            await bot.telegram.stopPoll(poll.chat_id, poll.message_id)
                .catch(async error => {
                    console.error(`Error to stop the poll #${poll.id} - ${error.message}.`)

                    await poll.update({ is_closed: 1 })
                })

            if (schedule.cancelJob(scheduleName)) {
                console.log(`Schedule ${scheduleName} closed.`)
            }
        })

    return true
}

/**
 * Change pin status of the poll
 * @param {Telegraf|Context} bot
 * @param {Poll|string} poll
 */
export async function changePollPinStatus(bot, poll) {
    if (
        bot &&
        bot.constructor?.name !== 'Telegraf' &&
        bot.constructor?.name !== 'Context'
    ) {
        throw Error('Invalid parameter: bot.')
    }

    if (typeof poll === 'string') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    if (poll.is_pinned === 0) {
        try {
            await bot.telegram.pinChatMessage(
                poll.chat_id,
                poll.message_id,
                { disable_notification: true }
            )
        } catch {
            console.error(`Error to pin the poll #${poll.id} - ${error.message}.`)
        }
    }
    else {
        await bot.telegram.unpinChatMessage(
            poll.chat_id,
            poll.message_id,
        )
    }

    await poll.update({
        is_pinned: poll.is_pinned ? 0 : 1
    })

    console.log(
        `Poll #${poll.id} was ` +
        (poll.is_pinned ? 'pinned.' : 'unpinned.')
    )

    return true
}

/**
 * Cancel poll
 * @param {Telegraf|Context} bot
 * @param {Poll|string} poll
 */
export async function cancelPoll(bot, poll) {
    if (
        bot &&
        bot.constructor?.name !== 'Telegraf' &&
        bot.constructor?.name !== 'Context'
    ) {
        throw Error('Invalid parameter: bot.')
    }

    if (typeof poll === 'string') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    await bot.telegram.stopPoll(poll.chat_id, poll.message_id)
        .catch(async error => {
            console.error(`Error to stop the poll #${poll.id} - ${error.message}.`)
        })

    await poll.update({ is_closed: 1 })

    schedule.cancelJob(`poll-reminder-${poll.id}`)
    schedule.cancelJob(`poll-stop-${poll.id}`)

    console.log(`Poll #${poll.id} canceled.`)

    return true
}

/**
 * Delete poll
 * @param {Telegraf|Context} bot
 * @param {Poll|string} poll
 */
export async function deletePoll(bot, poll) {
    if (
        bot &&
        bot.constructor?.name !== 'Telegraf' &&
        bot.constructor?.name !== 'Context'
    ) {
        throw Error('Invalid parameter: bot.')
    }

    if (typeof poll === 'string') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    await bot.telegram.unpinChatMessage(
        poll.chat_id,
        poll.message_id,
    )
        .catch(() => { })

    /** Try to remove description message in chat except MON */
    if (dayjs(poll.created_at).day !== 0) {
        await bot.telegram.deleteMessage(
            poll.chat_id,
            poll.message_id - 1,
        )
            .catch(() => { })
    }

    /** Remove main poll message in chat */
    await bot.telegram.deleteMessage(
        poll.chat_id,
        poll.message_id,
    )
        .catch(() => { })

    await poll.destroy()

    console.log(`Poll #${poll.id} deleted.`)

    return true
}
