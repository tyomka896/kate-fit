/**
 * Week controller
 */
import { Op } from 'sequelize'

import dayjs from '#utils/dayjs.js'
import * as emoji from '#utils/emoji.js'
import { Week } from '#models/week.js'

/**
 * Get active week by id
 * @param {number} weekId
 * @returns Week
 */
export async function getActiveWeek(weekId) {
    if (typeof weekId !== 'string') {
        throw Error('Invalid parameter: weekId.')
    }

    const weeks = await getActiveWeeks([weekId])

    return weeks.length ? weeks[0] : null
}

/**
 * Get active weeks by array if ids
 * @param {Array} weekIds
 * @returns Weeks
 */
export async function getActiveWeeks(weekIds) {
    if (!Array.isArray(weekIds)) {
        throw Error('Invalid parameter: weekIds.')
    }

    return await Week.findAll({
        where: {
            id: { [Op.in]: weekIds },
            workout_id: { [Op.not]: null },
            time: { [Op.not]: null },
            place: { [Op.not]: null },
            has_poll: 1,
        },
        include: ['workout'],
    })
}

/**
 * Message with the weekday description
 * @param {string|Week} week
 * @returns string
 */
export async function weekdayDescription(week) {
    if (typeof week === 'string') {
        week = await Week.findByPk(week, { include: 'workout' })
    }
    if (week?.constructor?.name !== 'Week') {
        throw Error('Invalid parameter: week.')
    }

    if (!(week.workout_id && week.time && week.place)) return null

    const workout = week.workout || await week.getWorkout()

    const weekDayPrep = week.id === 'TU' ? 'во' : 'в'

    const weekDayName = {
        'MON': 'Понедельник',
        'TU': 'Вторник',
        'WED': 'Среду',
        'TH': 'Четверг',
        'FRI': 'Пятницу',
        'SAT': 'Субботу',
        'SUN': 'Воскресенье',
    }[week.id]

    const trainWord = /трени(ров|нг)/.test(workout.name) ? '' : 'тренировка '

    const startAt = dayjs(week.time).tz()

    const rows = [
        `Доброго дня ${emoji.goodDay()}\n\n`,
        `${emoji.gender()} Уже завтра ${weekDayPrep} <b>${weekDayName}</b> ` +
        `состоится ${trainWord}<u><i>${workout.name}</i></u>.\n\n`,
        `⏱ <b>${startAt.format('HH:mm')}</b>\n`,
        `📍 ${week.place}`,
    ]

    if (workout.about) {
        rows.push(`\n\n📝 ${workout.about}`)
    }

    if (workout.extra) {
        rows.push(
            `\n\n👉 <b>Вам потребуется:</b>\n${workout.extra}`
        )
    }

    return rows.join('')
}

/**
 *
 * Message with week program description
 * @returns string
 */
export async function weekDescription() {
    const weeks = await Week.findAll({
        where: {
            workout_id: { [Op.not]: null },
            time: { [Op.not]: null },
            place: { [Op.not]: null },
        },
        include: ['workout'],
    })

    if (!weeks.length) return null

    const weekDayNames = {
        'MON': 'ПОНЕДЕЛЬНИК',
        'TU': 'ВТОРНИК',
        'WED': 'СРЕДА',
        'TH': 'ЧЕТВЕРГ',
        'FRI': 'ПЯТНИЦА',
        'SAT': 'СУББОТА',
        'SUN': 'ВОСКРЕСЕНЬЕ',
    }

    const rows = [
        `Доброго дня ${emoji.goodDay()}\n\n`,
        '🗓 <i>Расписание тренировок на следующую неделю:</i>\n\n',
    ]

    const days = Object.keys(weekDayNames)
        .reduce((red, elem) => {
            const week = weeks.find(_elem => _elem.id === elem)

            if (!week) return red

            const startAt = dayjs(week.time).tz()

            red.push(`${emoji.weekDay()} <b>${weekDayNames[week.id]}</b>`)

            if (!week.has_poll) {
                red.push(`\n${emoji.noEvents()}<u>ТРЕНИРОВКИ НЕ БУДЕТ</u>${emoji.noEvents()}\n\n`)

                return red
            }

            red.push(
                ` ⏱ ${startAt.format('HH:mm')} - ` +
                `<u>${week.workout.name}</u>\n` +
                `📍 ${week.place}\n`
            )

            if (week.workout.about) {
                red.push(`📝 ${week.workout.about}\n`)
            }

            red.push('\n')

            return red
        }, [])

    rows.push(...days.join(''))

    return rows.join('')
}
