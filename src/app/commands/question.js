/**
 * Ask a question fitness instructor
 */
import { getAuth } from '#controllers/userController.js'

export default async function(ctx) {
    if (ctx.chat.type !== 'private') {
        const botName = env('BOT_NAME', 'BOT_NAME')

        return ctx.reply(
            'Задать личный вопрос можно лично 👇\n',
            {
                ...Markup.inlineKeyboard([
                    Markup.button.url(
                        `Перейти в @${botName}`,
                        `https://t.me/${botName}?start=question`,
                    ),
                ]),
                reply_to_message_id: ctx.message.message_id,
            }
        )
    }

    const auth = await getAuth(ctx)

    return ctx.scene.enter(auth.role, { act: 'question' } )
}
