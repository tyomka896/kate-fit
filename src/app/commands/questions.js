/**
 * Ask a question fitness instructor
 */
import { getAuth } from '#controllers/userController.js'

export default async function(ctx, next) {
    if (ctx.chat.type !== 'private') return

    const auth = await getAuth(ctx)

    if (! auth.admin()) return next()

    return ctx.scene.enter('admin', { act: 'questions' } )
}
