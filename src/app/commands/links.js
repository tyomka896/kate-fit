/**
 * Links command
 */
import { Markup } from 'telegraf'

export default async function(ctx) {
    if (ctx.chat.type !== 'private') return

    return ctx.reply(
        'Полезные ссылки',
        Markup.inlineKeyboard([
            [
                Markup.button.url(
                    'Kate FIT Channel',
                    'https://t.me/+tJBoC-E4TSs5MzZi'
                )
            ],
            [
                Markup.button.url(
                    'Группа здоровья СБ ГХК',
                    'https://t.me/+_Uhq6BK1dpU5M2E6'
                )
            ],
        ])
    )
}
