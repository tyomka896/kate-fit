/**
 * Payment command
 */
import { Op } from 'sequelize'

import dayjs from '#utils/dayjs.js'
import { firstCapital } from '#utils/helpers.js'
import { Poll } from '#models/poll.js'
import { getAuth } from '#controllers/userController.js'

export default async function(ctx) {
    if (ctx.chat.type !== 'private') return

    const auth = await getAuth(ctx)

    if (! auth.admin()) return next()

    const rows = [
        '<b>Сводка о доходах</b>\n\n',
    ]

    let _dayjs = dayjs().utc().startOf('month').subtract(1, 'month')

    const previousMonthPayment = await Poll.sum('total_payment', {
        where: {
            [Op.and]: [
                { end_at: { [Op.gte]: _dayjs.format() } },
                { end_at: { [Op.lte]: _dayjs.endOf('month').format() } },
            ],
        },
    })

    rows.push(
        `• <i>${firstCapital(_dayjs.format('MMM, YYYY'))} г.</i>: ` +
        `${(previousMonthPayment || 0).toLocaleString()} ₽\n`
    )

    _dayjs = _dayjs.add(1, 'month')

    const currentMonthPayment = await Poll.sum('total_payment', {
        where: {
            [Op.and]: [
                { end_at: { [Op.gte]: _dayjs.format() } },
                { end_at: { [Op.lte]: _dayjs.endOf('month').format() } },
            ],
        },
    })

    rows.push(
        `• <i>${firstCapital(_dayjs.format('MMM, YYYY'))} г.</i>: ` +
        `${(currentMonthPayment || 0).toLocaleString()} ₽\n`
    )

    _dayjs = dayjs().startOf('year')

    const currentYearPayment = await Poll.sum('total_payment', {
        where: { end_at: { [Op.gte]: _dayjs.format() } },
    })

    rows.push(
        `• <u><i>За ${_dayjs.format('YYYY')} г.</i>: ` +
        `${(currentYearPayment || 0).toLocaleString()} ₽</u>\n`
    )

    const totalPayment = await Poll.sum('total_payment')

    rows.push(`• <i>Весь период</i>: ${(totalPayment || 0).toLocaleString()} ₽`)

    return ctx.reply(rows.join(''), { parse_mode: 'HTML' })
}
