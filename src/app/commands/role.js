/**
 * Role switching command
 */
import { User } from '#models/user.js'
import { getAuth } from '#controllers/userController.js'

/** Exception on who can change the role */
const USER_ID_EXCEPTION = 501244780

export default async function(ctx, next) {
    if (ctx.chat.type !== 'private') return

    const auth = await getAuth(ctx)

    if (+auth.id !== USER_ID_EXCEPTION && ! auth.admin()) return next()

    const params = ctx.message.text.split(' ').splice(1)

    if (params.length === 0) return next()

    let user = null
    const value = params[0].substring(1)
    const symbol = params[0].charAt(0)

    if (symbol === '#') {
        user = await User.findOne({ where: { id: value } })
    }
    else if (symbol === '@') {
        user = await User.findOne({ where: { username: value } })
    }
    else {
        return ctx.reply('Необходимо задать id или username.')
    }

    if (! user) {
        return ctx.reply('Пользователь не найден.')
    }

    if (+auth.id !== USER_ID_EXCEPTION && auth.id === user.id) {
        return ctx.reply('Нельзя изменить роль самому себе.')
    }

    await user.update({
        role: user.admin() ? 'user' : 'admin',
    })

    return ctx.reply(
        `Пользователю <i>${user.fullName(false)}</i> ` +
        `назначена роль <i>${user.role}</i>.`,
        { parse_mode: 'HTML' }
    )
}
