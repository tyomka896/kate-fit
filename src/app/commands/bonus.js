/**
 * Cancel last operation
 */
import { Op } from 'sequelize'

import dayjs from '#utils/dayjs.js'
import { numberToWords } from '#utils/helpers.js'
import { User } from '#models/user.js'
import { Poll } from '#models/poll.js'
import { UserHasPoll } from '#models/user_has_poll.js'
import { getAuth } from '#controllers/userController.js'

/** Bonuses multiple of value */
const BONUS_PER_VISITS = 8

export default async function(ctx) {
    const auth = await getAuth(ctx)

    const reply = {
        text: '',
        extra: { parse_mode: 'HTML' },
    }

    if (ctx.chat.type !== 'private') {
        reply.extra.reply_to_message_id = ctx.message.message_id
    }

    const poll = await Poll.findOne({
        where: {
            end_at: {
                [Op.gte]: dayjs().utc().startOf('month').format(),
            },
        },
        order: [ 'created_at' ],
    })

    if (! poll) {
        reply.text =
            'Вам нужно посетить <b>7 занятий</b>, ' +
            'чтобы получить бонусную тренировку в этом месяце.'

        return ctx.reply(reply.text, reply.extra)
    }

    const KateFitKv = await User.findByPk(1211871972)

    const visits = await UserHasPoll.findAll({
        where: {
            user_id: auth.id,
            [Op.or]: [
                { payment: { [Op.gt]: 0 } },
                { has_bonus: 1 },
            ],
            created_at: { [Op.gte]: poll.created_at },
        }
    })

    const visitsCount = visits.length
    const bonusesCount = visits.filter(elem => elem.has_bonus === 1).length

    const visitsLeft = BONUS_PER_VISITS - (visitsCount % BONUS_PER_VISITS + 1)

    // Round up visits left counter based on previous bonuses
    // if (BONUS_PER_VISITS * bonusesCount > visitsCount) {
    //     visitsLeft += BONUS_PER_VISITS * bonusesCount
    // }

    if (Math.floor(visitsCount / BONUS_PER_VISITS) > bonusesCount) {
        reply.text =
            'В этом месяце для вас <b>не засчитали</b> ' +
            'бонусную тренировку.\n' +
            `Напомните об этом ${KateFitKv.linkName()}, ` +
            'чтобы ее проставили.'
    }
    else if (visitsLeft === 0) {
        reply.text =
            'Следующее занятие для вас будет <b>бесплатным</b>.\n' +
            `После тренировки ${KateFitKv.linkName()} вас отметит.`
    }
    else {
        if (visitsCount > 0) {
            const visitsWord = numberToWords(visitsCount, 'тренировку_тренировки_тренировок')

            reply.text = `Вы посетили <b>${visitsCount} ${visitsWord}</b>. `
        }

        const leftWord = numberToWords(visitsLeft, 'занятие_занятия_занятий')
        const anotherBonus = bonusesCount > 0 ? ' <u><i>еще одну</i></u> ' : ' '

        reply.text +=
            `Вам осталось <b>${visitsLeft} ${leftWord}</b> ` +
            `чтобы получить${anotherBonus}бонусную тренировку в этом месяце.`
    }

    return ctx.reply(reply.text, reply.extra)
}
