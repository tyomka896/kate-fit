/**
 * Send week program to the chat
 */
import { getAuth } from '#controllers/userController.js'

export default async function(ctx) {
    if (ctx.chat.type !== 'private') return

    const auth = await getAuth(ctx)

    if (! auth.admin()) return next()

    return ctx.scene.enter('admin', { act: 'program' } )
}
