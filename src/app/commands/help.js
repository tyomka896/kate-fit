/**
 * Help command
 */
import { Markup } from 'telegraf'
import { getAuth } from '#controllers/userController.js'

export default async function (ctx) {
    if (ctx.chat.type !== 'private') {
        const botName = env('BOT_NAME', 'BOT_NAME')

        return ctx.reply(
            'Просмотреть все команды 👇\n',
            {
                ...Markup.inlineKeyboard([
                    Markup.button.url(
                        `Перейти в @${botName}`,
                        `https://t.me/${botName}?start=help`,
                    ),
                ]),
                reply_to_message_id: ctx.message.message_id,
            }
        )
    }

    const auth = await getAuth(ctx)

    const rows = [
        '<b>Команды</b>\n',
        '/start - начать диалог\n',
        '/bonus - когда бонусное занятие\n',
        '/question - задать личный вопрос\n',
        '/certs - сертификаты @KateFit\n',
        '/food_hygiene - формула Кэтча-МакАрдла\n',
        '/links - полезные ссылки\n',
        '/cancel - отмена последнего действия',
    ]

    if (auth.admin()) {
        rows.push('\n\n<b>Информация</b>\n')
        rows.push('/bonuses - ближайшие бонусники\n')
        rows.push('/questions - вопросы от пользователей\n')
        rows.push('/payment - сводка о доходах\n')
        rows.push('/status - небольшая статистика\n\n')

        rows.push('<b>Дополнительно</b>\n')
        rows.push('/settings - настройки\n')
        rows.push('/program - отправить расписание в чат\n\n')

        const botUrl = `https://t.me/${env('BOT_NAME', 'BOT_NAME')}?start`

        rows.push('<b>Ссылки к боте (зажать для копирования)</b>\n')
        rows.push(`• <a href="${botUrl}=question">Задать личный вопрос</a>\n`)
        rows.push(`• <a href="${botUrl}=food-hygiene">Тест Кэтча-МакАрдла</a>`)
    }

    return ctx.reply(rows.join(''), { parse_mode: 'HTML' })
}
