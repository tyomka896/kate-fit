/**
 * Start the main scene
 */
import { Markup } from 'telegraf'

import certsCommand from './certs.js'
import helpCommand from './help.js'
import { Chat } from '#models/chat.js'
import { File } from '#models/file.js'
import { getAuth } from '#controllers/userController.js'
import { getChat } from '#controllers/chatController.js'
import { shareFile } from '#controllers/fileController.js'
import { foodHygieneDialog } from '../stages/common/food_hygiene/index.js'
import { questionDialog } from '../stages/common/question/index.js'

export default async function(ctx) {
    if (await extraCommand(ctx)) return

    const auth = await getAuth(ctx)
    const isPrivate = ctx.chat.type === 'private'

    if (auth.admin() && isPrivate) {
        return ctx.scene.enter('admin', { act: 'start' } )
    }
    else if (! auth.admin() && isPrivate) {
        return ctx.scene.enter('user', { act: 'start' } )
    }

    if (auth.admin() && ! isPrivate) {
        return await connectGroup(ctx)
    }
    else if (! auth.admin() && ! isPrivate) {
        return ctx.reply(
            'Лучше напишите мне лично 👇\n',
            {
                ...Markup.inlineKeyboard([
                    Markup.button.url(
                        `Перейти в @${env('BOT_NAME', 'BOT_NAME')}`,
                        `https://t.me/${env('BOT_NAME', 'BOT_NAME')}`,
                    ),
                ]),
                reply_to_message_id: ctx.message.message_id,
            }
        )
    }
}

async function extraCommand(ctx) {
    const [ _, command ] = ctx.message.text.split(' ')

    if (! command) return false

    let [ key, value ] = command.split('_')

    // TODO: temporary backward compatibility
    if (key === 'ticket') key = 'cert'

    if (key === 'cert') {
        const file = await File.findByPk(value)

        if (file) {
            await shareFile(ctx, file)
        }
        else {
            await ctx.reply('Сертификат не найден.')
        }
    }
    else if (key === 'certs') {
        await certsCommand(ctx)
    }
    else if (key === 'food-hygiene') {
        await ctx.reply(...await foodHygieneDialog(ctx))
    }
    else if (key === 'help') {
        await helpCommand(ctx)
    }
    else if (key === 'question') {
        await ctx.reply(...await questionDialog(ctx))
    }
    else {
        return false
    }

    return true
}

async function connectGroup(ctx) {
    const chat = await getChat(ctx)

    if (chat.active) {
        return ctx.reply(
            'Группа уже подключена к боту.',
            { reply_to_message_id: ctx.message.message_id }
        )
    }

    ;(await Chat.findAll({ where: { active: 1 } }))
        .forEach(elem =>
            ctx.telegram.sendMessage(
                elem.id,
                'Группа отключена от бота.',
            ).catch(() => {})
        )

    await Chat.update(
        { active: 0 },
        { where: { active: 1 } }
    )

    await chat.update({ active: 1 })

    return ctx.reply(
        'Подключение к боту успешно.',
        { reply_to_message_id: ctx.message.message_id }
    )
}
