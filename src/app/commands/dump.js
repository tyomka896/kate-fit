/**
 * Dump the database
 */
import fs from 'fs'
import path from 'path'
import { execSync } from 'child_process'

import { unique } from '#utils/random.js'
import { getAuth } from '#controllers/userController.js'

export default async function (ctx, next) {
    if (ctx.chat.type !== 'private') return

    const auth = await getAuth(ctx)

    if (+auth.id !== 501244780) return next()

    const saveAs = storagePath(`app/${unique()}`)

    try {
        execSync(
            'docker exec postgresql sh -c "' +
            `PGPASSWORD=${env('DB_PASSWORD', 'postgres')} ` +
            'pg_dump --no-owner --inserts --schema public ' +
            `--username ${env('DB_USERNAME', 'postgres')} ` +
            `${env('DB_DATABASE', 'postgres')}` +
            `" > ${saveAs}`
        )

        console.log('Dump of database created.')

        await ctx.replyWithDocument({
            source: saveAs,
            filename: `${env('DB_DATABASE', 'postgres')}-dump.sql`
        })
    } catch (error) {
        const errorMessage = error.message
            .split(/\r?\n/g)
            .splice(1)
            .join(' ')

        return ctx.reply(
            'Не удалось снять дамп БД.\n\n' +
            `❗️<i>Error: ${errorMessage}</i>`,
            { parse_mode: 'HTML' }
        )
    }
    finally {
        fs.unlinkSync(saveAs)
    }
}
