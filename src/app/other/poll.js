/**
 * Native poll handler
 */
import { Markup } from 'telegraf'

import { sleep } from '#utils/helpers.js'
import { User } from '#models/user.js'
import { Poll } from '#models/poll.js'
import { Status } from '#models/status.js'
import { getAuth } from '#controllers/userController.js'
import { createUser } from '#controllers/userController.js'
import { getStatusPoll } from '#controllers/pollController.js'
import { createOrUpdateUserHasPoll } from '#controllers/userHasPollController.js'
import { pollKeyboard } from '../stages/admin/start/polls/edit.js'

/**
 * Bot on poll
 * @param {Context} ctx
 */
async function onPoll(ctx) {
    const model = await Poll.findByPk(ctx.poll.id)

    if (! model) return

    await model.update({
        options: JSON.stringify(ctx.poll.options),
        total_voters: ctx.poll.total_voter_count,
        is_closed: +ctx.poll.is_closed,
    })

    if (ctx.poll.is_closed) {
        const _options = ctx.poll.options
            .map(elem => delete elem.text && elem)

        await model.update({ options: JSON.stringify(_options) })

        await updatePollStatus(ctx, model)

        await Status.destroy({
            where: { poll_id: model.id }
        })
    }
}

/**
 * Bot on poll_answer
 * @param {Context} ctx
 */
async function onPollAnswer(ctx) {
    /** Delay before onPoll completed */
    await sleep(300)

    const userInfo = ctx.pollAnswer.user

    if (userInfo.is_bot) return

    const poll = await Poll.findByPk(ctx.pollAnswer.poll_id)

    if (! poll) return

    const user =
        await User.findByPk(userInfo.id) ||
        await createUser({
            id: userInfo.id,
            username: userInfo.username,
            first_name: userInfo.first_name,
            last_name: userInfo.last_name,
        })

    await createOrUpdateUserHasPoll(user, poll, ctx.pollAnswer.option_ids)

    await updatePollStatus(ctx, poll)
}

async function pollStatusGoTo(ctx) {
    const modelId = ctx.match[1]

    const model = await Poll.findByPk(modelId)

    if (! model) return

    ctx.session.pollId = model.id

    await ctx.editMessageText(...await pollKeyboard(ctx))

    const auth = await getAuth(ctx)

    await Status.destroy({
        where: {
            user_id: auth.id,
            poll_id: model.id,
        }
    })
}

/**
 * Update poll status messages
 * @param {Context} ctx
 * @param {string} poll
 * @returns
 */
async function updatePollStatus(ctx, poll) {
    if (typeof poll === 'string') {
        poll = await Poll.findByPk(poll)
    }
    if (poll?.constructor?.name !== 'Poll') {
        throw Error('Invalid parameter: poll.')
    }

    const statuses = await poll.getStatuses()

    const currentStatus = await getStatusPoll(poll)

    const markup = Markup.inlineKeyboard([
        [
            Markup.button.callback(
                'К опросу »',
                `poll-status-go-to-${poll.id}`,
            ),
        ],
    ])

    for (let status of statuses) {
        try {
            await ctx.telegram.editMessageText(
                status.user_id,
                status.message_id,
                null,
                currentStatus,
                { ...markup, parse_mode: 'HTML' }
            )
        } catch {
            await status.destroy()
        }
    }
}

/**
 * Init poll handlers
 * @param {Telegraf} bot
 */
export default function(bot) {
    bot.on('poll', onPoll)

    bot.on('poll_answer', onPollAnswer)

    bot.action(/poll-status-go-to-(\d*)/, pollStatusGoTo)
}
