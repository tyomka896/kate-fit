/**
 * Daily Energy Expenditure with Katch-McArdle formula
 */

/**
 * Active coefficient based on activity type
 * @param {number} value
 * @returns
 */
function activeCoefficient(value) {
    switch (value) {
        case 1: return 1.2   // Passive lifestyle
        case 2: return 1.375 // Light activity
        case 3: return 1.55  // Middle activity
        case 4: return 1.725 // Hight activity
        case 5: return 1.9   // Extreme activity
        default: return 0
    }
}

/**
 * Body mass by four different calculations
 * @param {boolean} sex_woman
 * @param {number} height
 * @returns
 */
function idealBodyMass(sex_woman, height) {
    return [
        // G.J. Hamwi formula (1964)
        sex_woman ? 45.5 + 2.2 * (height / 2.54 - 60) : 48 + 2.7 * (height / 2.54 - 60),
        // B.J. Devine formula (1974)
        sex_woman ? 45.5 + 2.3 * (height / 2.54 - 60) : 50 + 2.3 * (height / 2.54 - 60),
        // J.D. Robinson formula (1983)
        sex_woman ? 49 + 1.7 * (height / 2.54 - 60) : 52 + 1.9 * (height / 2.54 - 60),
        // D.R. Miller formula (1983)
        sex_woman ? 53.1 + 1.36 * (height / 2.54 - 60) : 56.2 + 1.41 * (height / 2.54 - 60),
    ]
}

/**
 * Basic daily energy expenditure calculation
 * @param {object} options
 * @param {boolean} options.sex_woman is person a woman
 * @param {number} options.age full age
 * @param {number} options.weight weight in kg
 * @param {number} options.height height in kg
 * @param {number} options.neck neck volume in cm
 * @param {number} options.waist waist volume in cm
 * @param {number} options.hip hip volume in cm
 * @param {number} options.active daily activity type
 * @returns
 */
export function dailyEnergyExpenditure(options) {
    const { sex_woman, age, weight, height, neck, waist, hip, active } = options

    const body_mass_index = weight / Math.pow(height / 100, 2)

    const ideal_body_mass = idealBodyMass(sex_woman, height)

    let active_coefficient = activeCoefficient(active)

    let body_fat_percentage = 0

    if (sex_woman) {
        body_fat_percentage =
            495 / (1.29579 - 0.35004 * (Math.log10(waist + hip - neck)) +
            0.22100 * Math.log10(height)) - 450
    }
    else {
        body_fat_percentage =
            495 / (1.0324 - 0.19077 * Math.log10(waist - neck) +
            0.15456 * Math.log10(height)) - 450
    }

    const lean_body_mass = (weight * (100 - body_fat_percentage)) / 100

    const basal_metabolic_rate = 21.6 * lean_body_mass + 370

    let resting_metabolic_rate = 0

    if (sex_woman) {
        resting_metabolic_rate = 447.593 + (9.247 * weight) + (3.098 * height) - (4.330 * age)
    }
    else {
        resting_metabolic_rate = 88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age)
    }

    const daily_energy_expenditure = basal_metabolic_rate * active_coefficient

    const daily_energy_expenditures = {}

    for (let l = 1; l <= 5; l++) {
        daily_energy_expenditures[l] = basal_metabolic_rate * activeCoefficient(l)
    }

    return {
        body_mass_index,
        ideal_body_mass,
        active_coefficient,
        body_fat_percentage,
        lean_body_mass,
        basal_metabolic_rate,
        resting_metabolic_rate,
        daily_energy_expenditure,
        daily_energy_expenditures,
    }
}

/**
 * Body fat based on sex, age and fat percentage
 * @param {boolean} is_woman
 * @param {number} age
 * @param {number} body_fat
 */
export function bodyFatText(is_woman, age, body_fat) {
    let agesInfo = []

    if (is_woman) {
        agesInfo = [
            {
                min: 0,
                max: 30,
                body_fat: {
                    lowest:  { min: 0, max: 16 },
                    low:     { min: 16, max: 20 },
                    middle:  { min: 20, max: 29 },
                    high:    { min: 29, max: 32 },
                    highest: { min: 32, max: Infinity },
                },
            },
            {
                min: 30,
                max: 40,
                body_fat: {
                    lowest:  { min: 0, max: 17 },
                    low:     { min: 17, max: 21 },
                    middle:  { min: 21, max: 30 },
                    high:    { min: 30, max: 33 },
                    highest: { min: 33, max: Infinity },
                },
            },
            {
                min: 40,
                max: 50,
                body_fat: {
                    lowest:  { min: 0, max: 18 },
                    low:     { min: 18, max: 22 },
                    middle:  { min: 22, max: 31 },
                    high:    { min: 31, max: 34 },
                    highest: { min: 34, max: Infinity },
                },
            },
            {
                min: 50,
                max: 60,
                body_fat: {
                    lowest:  { min: 0, max: 19 },
                    low:     { min: 19, max: 23 },
                    middle:  { min: 23, max: 32 },
                    high:    { min: 32, max: 34 },
                    highest: { min: 34, max: Infinity },
                },
            },
            {
                min: 60,
                max: Infinity,
                body_fat: {
                    lowest:  { min: 0, max: 20 },
                    low:     { min: 20, max: 24 },
                    middle:  { min: 24, max: 33 },
                    high:    { min: 33, max: 36 },
                    highest: { min: 36, max: Infinity },
                },
            },
        ]
    }
    else {
        agesInfo = [
            {
                min: 0,
                max: 30,
                body_fat: {
                    lowest:  { min: 0, max: 11 },
                    low:     { min: 11, max: 15 },
                    middle:  { min: 15, max: 22 },
                    high:    { min: 22, max: 25 },
                    highest: { min: 25, max: Infinity },
                },
            },
            {
                min: 30,
                max: 40,
                body_fat: {
                    lowest:  { min: 0, max: 12 },
                    low:     { min: 12, max: 15 },
                    middle:  { min: 15, max: 22 },
                    high:    { min: 22, max: 25 },
                    highest: { min: 25, max: Infinity },
                },
            },
            {
                min: 40,
                max: 50,
                body_fat: {
                    lowest:  { min: 0, max: 14 },
                    low:     { min: 14, max: 17 },
                    middle:  { min: 17, max: 24 },
                    high:    { min: 24, max: 27 },
                    highest: { min: 27, max: Infinity },
                },
            },
            {
                min: 50,
                max: 60,
                body_fat: {
                    lowest:  { min: 0, max: 15 },
                    low:     { min: 15, max: 18 },
                    middle:  { min: 18, max: 25 },
                    high:    { min: 25, max: 28 },
                    highest: { min: 28, max: Infinity },
                },
            },
            {
                min: 60,
                max: Infinity,
                body_fat: {
                    lowest:  { min: 0, max: 16 },
                    low:     { min: 16, max: 19 },
                    middle:  { min: 19, max: 26 },
                    high:    { min: 26, max: 29 },
                    highest: { min: 29, max: Infinity },
                },
            },
        ]
    }

    let bodyFat = {}
    
    for (let ageInfo of agesInfo) {
        if (age >= ageInfo.min && age < ageInfo.max) {
            bodyFat = ageInfo.body_fat
            
            break
        }
    }

    let emoji = ''
    const { lowest, low, middle, high, highest } = bodyFat

    if (body_fat >= lowest.min && body_fat < lowest.max) {
        emoji = '🦴'
    }
    else if (body_fat >= low.min && body_fat < low.max) {
        emoji = '🫛'
    }
    else if (body_fat >= middle.min && body_fat < middle.max) {
        emoji = '🥗'
    }
    else if (body_fat >= high.min && body_fat < high.max) {
        emoji = '🍫'
    }
    else if (body_fat >= highest.min && body_fat < highest.max) {
        emoji = '🍔'
    }

    bodyFat.lowest.text  = 'очень низкий'
    bodyFat.low.text     = 'низкий'
    bodyFat.middle.text  = 'оптимальный'
    bodyFat.high.text    = 'высокий'
    bodyFat.highest.text = 'очень высокий'

    const list = Object.keys(bodyFat)
        .map(key => {
            const elem = bodyFat[key]
            const isSelected = body_fat >= elem.min && body_fat < elem.max

            let sizeText = ''

            if (elem.max === Infinity) {
                sizeText = `${elem.min} и более`
            }
            else {
                sizeText = `${elem.min} - ${elem.max}`
            }

            return (isSelected ? '✓' : ' • ') +
                (isSelected ? '<u>' : '') +
                `${sizeText} : ${elem.text}` +
                (isSelected ? '</u>\n' : '\n')
        })

    return { emoji, text: list.join('') }
}



/**
 * Expenditures list
 * @param {number} daily
 * @param {number[]} list
 * @returns
 */
export function expendituresText(daily, list) {
    const activities = [
        'Сидячий образ',
        'Легкая активность',
        'Средняя активность',
        'Высокая активность',
        'Экстремальная активность',
    ]

    return Object.keys(list)
        .map((key, index) => {
            const value = list[key]
            const isSelected = value === daily

            return (isSelected ? '✓' : ' • ') +
                (isSelected ? '<u>' : '') +
                `${activities[index]} : ${Math.round(value)} ккал/день` +
                (isSelected ? '</u>\n' : '\n')
        })
        .join('')
}

/**
 * Obesity info
 * @param {number} value
 * @returns
 */
export function obesityClassText(value) {
    const classes = [
        { min: -Infinity, max: 16, text: 'Выраженный дефицит массы тела' },
        { min: 16, max: 18.5, text: 'Недостаточная (дефицит) масса тела' },
        { min: 18.5, max: 25, text: 'Нормальный вес' },
        { min: 25, max: 30, text: 'Избыточная масса тела (предожирение)' },
        { min: 30, max: 35, text: 'Ожирение первой степени' },
        { min: 35, max: Infinity, text: 'Ожирение второй степени' },
        // { min: 40, max: Infinity, text: 'Ожирение третьей степени (морбидное)' },
    ]

    return classes
        .map(elem => {
            const isSelected = value >= elem.min && value < elem.max

            let sizeText = ''

            if (elem.min === -Infinity) {
                sizeText = `${elem.max} и менее`
            }
            else if (elem.max === Infinity) {
                sizeText = `${elem.min} и более`
            }
            else {
                sizeText = `${elem.min} - ${elem.max}`
            }

            return (isSelected ? '✓' : ' • ') +
                (isSelected ? '<u>' : '') +
                `${sizeText} : ${elem.text}` +
                (isSelected ? '</u>\n' : '\n')
        })
        .join('')
}
