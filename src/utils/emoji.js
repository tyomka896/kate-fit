/**
 * Definition of how emoji are displayed in messages
 */

import dayjs from '#utils/dayjs.js'
import { random } from './random.js'

function isNewYear() {
    return [11, 0].includes(dayjs().utc().month())
}

/**
 * Random gender
 * @returns
 */
export function gender() {
    if (isNewYear()) {
        return '🎃'
    }

    return random(['🧘‍♀️', '🧘‍♂️'])
}

/**
 * Good day
 * @returns
 */
export function goodDay() {
    if (isNewYear()) {
        return '⛄️'
    }

    return '👋'
}

/**
 * Week day
 * @returns
 */
export function weekDay() {
    if (isNewYear()) {
        return '❄️'
    }

    return '•'
}

/**
 * No day events
 * @returns
 */
export function noEvents() {
    if (isNewYear()) {
        return '🎄'
    }

    return '❗️'
}
